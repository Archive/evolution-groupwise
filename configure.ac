dnl Sets the evolution groupware version.
m4_define([egw_version], [3.5.3])

dnl Initializes automake/autoconf
AC_PREREQ(2.58)
AC_INIT([evolution-groupwise], [egw_version], [http://bugzilla.gnome.org/enter_bug.cgi?product=Evolution%20Groupwise])
AM_INIT_AUTOMAKE([gnu 1.9 dist-xz no-dist-gzip -Wno-portability])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR(README)
AC_CONFIG_HEADERS(config.h)

dnl ****************************
dnl Evolution-GW API version
dnl ****************************
m4_define([EVO_GW_API_VERSION_MACRO], [3.2])
EVO_GW_API_VERSION=EVO_GW_API_VERSION_MACRO
AC_SUBST(EVO_GW_API_VERSION)

dnl Some requirements have versioned package names
dnl XXX In the spirit of getting rid of versioned
dnl     files, can we please drop these suffixes?
EDS_PACKAGE=1.2

dnl Automake 1.11 - Silent Build Rules
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

dnl Required Package Versions
m4_define([glib_minimum_version], [2.16.0])
m4_define([eds_minimum_version], [egw_version])
m4_define([evo_minimum_version], [egw_version])
m4_define([gconf_minimum_version], [2.0.0])
m4_define([gtk_minimum_version], [2.90.4])
m4_define([libxml_minimum_version], [2.0.0])
m4_define([libsoup_minimum_version], [2.3.0])

dnl ******************************
dnl Libtool versioning
dnl ******************************

LIBEGROUPWISE_CURRENT=13
LIBEGROUPWISE_REVISION=1
LIBEGROUPWISE_AGE=0

AC_SUBST(LIBEGROUPWISE_CURRENT)
AC_SUBST(LIBEGROUPWISE_REVISION)
AC_SUBST(LIBEGROUPWISE_AGE)

dnl *********************************************************************
dnl Update these for every new development release of Evolution-Exchange.
dnl These numbers actually correspond to the next stable release number.
dnl Note, this is set the way it is so that GETTEXT_PACKAGE will be
dnl parsed correctly.
dnl ******************************************************************
BASE_VERSION=3.6
m4_define([base_version], [3.6])
AC_SUBST(BASE_VERSION)	

dnl **********************
dnl Compiler Warning Flags
dnl **********************
AS_COMPILER_FLAGS(WARNING_FLAGS,
	"-DE_BOOK_DISABLE_DEPRECATED
	-DE_CAL_DISABLE_DEPRECATED
	-Wall -Wextra
	-Wno-missing-field-initializers
	-Wno-sign-compare
	-Wno-unused-parameter
	-Wno-deprecated-declarations
	-Wdeclaration-after-statement
	-Werror-implicit-function-declaration
	-Wformat-security -Winit-self
	-Wmissing-declarations -Wmissing-include-dirs
	-Wmissing-noreturn -Wnested-externs -Wpointer-arith
	-Wredundant-decls -Wundef -Wwrite-strings")
AC_SUBST(WARNING_FLAGS)

dnl Other useful compiler warnings for test builds only.
dnl These may produce warnings we have no control over,
dnl or false positives we don't always want to see.
dnl
dnl	-Wformat-nonliteral
dnl	-Wmissing-format-attribute
dnl	-Wshadow
dnl	-Wstrict-aliasing=2

AM_CPPFLAGS="$WARNING_FLAGS -fno-strict-aliasing"
AC_SUBST(AM_CPPFLAGS)

dnl **************************
dnl Initialize maintainer mode
dnl **************************
AM_MAINTAINER_MODE

AC_PROG_CC
AM_PROG_CC_C_O
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET

dnl GCC 4.4 got more aggressive in its aliasing optimizations, changing
dnl behavior that -- according to the C99 standard -- is supposed to be
dnl undefined.  We may still have aliasing abuses lying around that rely
dnl on GCC's previous "undefined" behavior, so disable strict-aliasing
dnl optimization until we can find and fix all the abuses.
dnl (AC_PROG_CC must run first to set the GCC variable.)
dnl XXX This really belongs in AM_CFLAGS.
if test "x${GCC}" = "xyes"; then
	CFLAGS="$CFLAGS -fno-strict-aliasing"
fi

dnl ******************************
dnl Check for Win32
dnl ******************************

AC_MSG_CHECKING([for Win32])
case "$host" in
*-mingw*)
	os_win32=yes
	NO_UNDEFINED='-no-undefined'
	SOCKET_LIBS='-lws2_32'
	DNS_LIBS='-ldnsapi'
	;;
*)
	os_win32=no
	NO_UNDEFINED='-no-undefined'
	SOCKET_LIBS=''
	DNS_LIBS=''
	;;
esac

AC_MSG_RESULT([$os_win32])
AM_CONDITIONAL(OS_WIN32, [test "x$os_win32" = "xyes"])
AC_SUBST(NO_UNDEFINED)
AC_SUBST(SOCKET_LIBS)
AC_SUBST(DNS_LIBS)

dnl ******************
dnl Initialize libtool
dnl ******************
LT_PREREQ(2.2)
LT_INIT(disable-static win32-dll)

PKG_PROG_PKG_CONFIG

dnl ***************
dnl Initialize i18n
dnl ***************
IT_PROG_INTLTOOL([0.35.5])
AM_GLIB_GNU_GETTEXT

GETTEXT_PACKAGE=evolution-groupwise
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [Package name for gettext])

localedir='$(prefix)/$(DATADIRNAME)/locale'
AC_SUBST(localedir)

dnl **********************************
dnl Check for base dependencies early.
dnl **********************************

PKG_CHECK_MODULES(GNOME_PLATFORM,
	[gio-2.0 >= glib_minimum_version
	gtk+-3.0 >= gtk_minimum_version
	gconf-2.0 >= gconf_minimum_version
	libxml-2.0 >= libxml_minimum_version
	libsoup-2.4 >= libsoup_minimum_version])

dnl *****************************************
dnl Check for evolution-data-server libraries 
dnl *****************************************
PKG_CHECK_MODULES(EVOLUTION_DATA_SERVER, evolution-data-server-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBEDATASERVER, libedataserver-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBEDATASERVERUI, libedataserverui-3.0 >= eds_minimum_version)
PKG_CHECK_MODULES(LIBEBACKEND, libebackend-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBECAL, libecal-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBEDATACAL, libedata-cal-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBBOOK, libebook-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(LIBEDATABOOK, libedata-book-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(CAMEL, camel-$EDS_PACKAGE >= eds_minimum_version)
PKG_CHECK_MODULES(SOUP, libsoup-2.4 >= libsoup_minimum_version)

dnl ************************
dnl Check for socklen_t type
dnl ************************
if test x$os_win32 != xyes; then
   AC_EGREP_HEADER(socklen_t, sys/socket.h, :, AC_DEFINE(socklen_t, int, [Define to "int" if socklen_t is not defined]))
fi

dnl ******************************
dnl Posix thread support
dnl ******************************

AC_DEFINE(ENABLE_THREADS,1,[Required])

dnl *******************
dnl GObject marshalling
dnl *******************
AM_PATH_GLIB_2_0

dnl ***********
dnl GConf stuff
dnl ***********
AC_PATH_PROG([GCONFTOOL],[gconftool-2],[no])
AM_GCONF_SOURCE_2


dnl ******************************
dnl Groupwise flags
dnl ******************************
LIBSOUP_REQUIRED=libsoup_minimum_version
AC_SUBST(LIBSOUP_REQUIRED)

dnl *************
dnl Gtk-Doc stuff
dnl *************
GTK_DOC_CHECK(1.9)

gwprivdatadir='${datadir}'/evolution/$BASE_VERSION
AC_SUBST(gwprivdatadir)

uidir="$gwprivdatadir/ui"
AC_SUBST(uidir)

camel_providerdir='${libdir}'/evolution-data-server/camel-providers
AC_SUBST(camel_providerdir)

plugindir="`$PKG_CONFIG --variable=plugindir evolution-plugin-3.0`"
AC_SUBST(plugindir)

EVOLUTION_PLUGIN_errordir="`$PKG_CONFIG --variable=errordir evolution-plugin-3.0`"
AC_SUBST(EVOLUTION_PLUGIN_errordir)

ebook_backenddir=`$PKG_CONFIG --variable=backenddir libedata-book-1.2`
AC_SUBST(ebook_backenddir)

ecal_backenddir=`$PKG_CONFIG --variable=backenddir libedata-cal-1.2`
AC_SUBST(ecal_backenddir)

EVOLUTION_imagesdir="`$PKG_CONFIG --variable=imagesdir evolution-shell-3.0`"
AC_SUBST(EVOLUTION_imagesdir)

CAMEL_providerdir="`$PKG_CONFIG --variable=camel_providerdir camel-1.2`"
AC_SUBST(CAMEL_providerdir)

ebook_backenddir=`$PKG_CONFIG --variable=backenddir libedata-book-1.2`
AC_SUBST(ebook_backenddir)

ecal_backenddir=`$PKG_CONFIG --variable=backenddir libedata-cal-1.2`
AC_SUBST(ecal_backenddir)

camel_providerdir=`$PKG_CONFIG --variable=camel_providerdir camel-$EDS_PACKAGE`
AC_SUBST(camel_providerdir)

edataserver_privincludedir=`$PKG_CONFIG --variable=privincludedir libedataserver-$EDS_PACKAGE`
AC_SUBST(edataserver_privincludedir)

dnl ***************************
dnl Check for evolution plugins
dnl ***************************
PKG_CHECK_MODULES(EVOLUTION_PLUGIN, evolution-plugin-3.0 >= evo_minimum_version)
AC_SUBST(EVOLUTION_PLUGIN_CFLAGS)
AC_SUBST(EVOLUTION_PLUGIN_LIBS)

dnl ***************************
dnl Check for evolution mail
dnl ***************************
PKG_CHECK_MODULES(EVOLUTION_MAIL, evolution-mail-3.0 >= evo_minimum_version)
AC_SUBST(EVOLUTION_MAIL_CFLAGS)
AC_SUBST(EVOLUTION_MAIL_LIBS)

dnl ***************************
dnl Check for evolution shell
dnl ***************************
PKG_CHECK_MODULES(EVOLUTION_SHELL, evolution-shell-3.0 >= evo_minimum_version)
AC_SUBST(EVOLUTION_SHELL_CFLAGS)
AC_SUBST(EVOLUTION_SHELL_LIBS)

AC_DEFINE([HANDLE_LIBICAL_MEMORY],[1],[Define it once memory returned by libical is free'ed properly])

dnl **************
dnl libdb checking
dnl **************
AC_ARG_WITH([libdb],
        AC_HELP_STRING([--with-libdb=PREFIX],
        [Prefix where libdb is installed]),
        [libdb_prefix="$withval"], [libdb_prefix='${prefix}'])

DB_CFLAGS="-I$libdb_prefix/include"
DB_LIBS="-L$libdb_prefix/lib -ldb"

AC_MSG_CHECKING([Berkeley DB])
save_cflags=$CFLAGS
save_libs=$LIBS
CFLAGS=$DB_CFLAGS
LIBS="$DB_LIBS"
AC_LINK_IFELSE([AC_LANG_PROGRAM(
        [[#include <db.h>]],
        [db_create(NULL, NULL, 0)])],
        [check_db=yes],
        [check_db=no])

if test "x$check_db" = "xyes"; then
        AC_MSG_RESULT([$check_db])
else
        AC_DEFINE(ENABLE_CACHE, 0, [Disabling GAL Caching])
        DB_CFLAGS=""
        DB_LIBS=""
fi
AM_CONDITIONAL(HAVE_LIBDB, [test "x$check_db" = "xyes"])

CFLAGS=$save_cflags
LIBS=$save_libs
AC_SUBST(DB_CFLAGS)
AC_SUBST(DB_LIBS)

dnl *******************************
dnl Add evolution plugin rules here
dnl *******************************
EVO_PLUGIN_RULE=$srcdir/eplugin-rule.mk
AC_SUBST_FILE(EVO_PLUGIN_RULE)

dnl ******************************
dnl Makefiles
dnl ******************************

AC_CONFIG_FILES([po/Makefile.in
Makefile
src/Makefile
src/server/Makefile
src/plugins/Makefile
src/calendar/Makefile
src/addressbook/Makefile
src/camel/Makefile
src/camel/groupwise/Makefile
src/camel/gwsend/Makefile
src/camel/imapx/Makefile
src/server/libegroupwise-]EVO_GW_API_VERSION_MACRO[.pc:src/server/libegroupwise.pc.in
])
AC_OUTPUT

AC_MSG_NOTICE([
	GTK-Doc			: $enable_gtk_doc
])
