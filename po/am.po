# Translations into the Amharic Language.
# Copyright (C) 2002 Free Software Foundation, Inc.
# This file is distributed under the same license as the evolution package.
# Ge'ez Frontier Foundation <locales@geez.org>, 2002.
#
#
msgid ""
msgstr ""
"Project-Id-Version: evolution 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-05-11 14:35+0530\n"
"PO-Revision-Date: 2003-02-07 17:46+EDT\n"
"Last-Translator: Ge'ez Frontier Foundation <locales@geez.org>\n"
"Language-Team: Amharic <locales@geez.org>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/addressbook/e-book-backend-groupwise.c:2261
#: ../src/addressbook/e-book-backend-groupwise.c:2283
#: ../src/addressbook/e-book-backend-groupwise.c:2328
#, fuzzy
msgid "Searching..."
msgstr "&ፈልግ"

#: ../src/addressbook/e-book-backend-groupwise.c:2330
msgid "Loading..."
msgstr "በመጫን ላይ..."

#: ../src/addressbook/e-book-backend-groupwise.c:2699
#, c-format
msgid "Downloading contacts (%d)... "
msgstr ""

#: ../src/addressbook/e-book-backend-groupwise.c:2849
#: ../src/addressbook/e-book-backend-groupwise.c:3030
#: ../src/addressbook/e-book-backend-groupwise.c:3077
#, c-format
msgid "Updating contacts cache (%d)... "
msgstr ""

#: ../src/calendar/e-cal-backend-groupwise-utils.c:1130
#: ../src/camel/camel-groupwise-folder.c:2114
msgid "Reply Requested: by "
msgstr ""

#: ../src/calendar/e-cal-backend-groupwise-utils.c:1135
#: ../src/camel/camel-groupwise-folder.c:2119
msgid "Reply Requested: When convenient"
msgstr ""

#: ../src/calendar/e-cal-backend-groupwise.c:329
#, fuzzy
msgid "Loading Appointment items"
msgstr "የቀን መቁጠሪያ ኩነቶች"

#: ../src/calendar/e-cal-backend-groupwise.c:331
#, fuzzy
msgid "Loading Task items"
msgstr "በመጫን ላይ..."

#: ../src/calendar/e-cal-backend-groupwise.c:333
#, fuzzy
msgid "Loading Note items"
msgstr "ዶሴን አስወግድ"

#: ../src/calendar/e-cal-backend-groupwise.c:335
#, fuzzy
msgid "Loading items"
msgstr "ዶሴን አስወግድ"

#: ../src/calendar/e-cal-backend-groupwise.c:951
#: ../src/plugins/camel-gw-listener.c:427
#: ../src/plugins/camel-gw-listener.c:452
#: ../src/plugins/camel-gw-listener.c:568
msgid "Calendar"
msgstr "ቀን መቁጠሪያ"

#: ../src/calendar/e-cal-backend-groupwise.c:1014
#, fuzzy
msgid "Invalid server URI"
msgstr "ጥያቄ"

#: ../src/calendar/e-cal-backend-groupwise.c:1115
#: ../src/calendar/e-cal-backend-groupwise.c:1361
#, fuzzy
msgid "Could not create cache file"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/calendar/e-cal-backend-groupwise.c:1127
#, fuzzy
msgid "Could not create thread for populating cache"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-folder.c:958
#, fuzzy, c-format
msgid "Could not load summary for %s"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-folder.c:1054
#, fuzzy, c-format
msgid "Checking for deleted messages %s"
msgstr "ሴኔጋል"

#: ../src/camel/camel-groupwise-folder.c:1314
#: ../src/camel/camel-groupwise-folder.c:1332
#: ../src/camel/camel-groupwise-store.c:566
#: ../src/camel/camel-groupwise-store.c:748
#: ../src/camel/camel-groupwise-transport.c:112
#: ../src/camel/camel-groupwise-transport.c:128
#, fuzzy, c-format
msgid "Authentication failed"
msgstr "_ቦታ"

#: ../src/camel/camel-groupwise-folder.c:1530
#: ../src/camel/camel-groupwise-store.c:603
#: ../src/camel/camel-groupwise-store.c:734
#, c-format
msgid "Fetching summary information for new messages in %s"
msgstr ""

#: ../src/camel/camel-groupwise-folder.c:2000
#: ../src/camel/camel-groupwise-folder.c:2053
#: ../src/camel/camel-groupwise-folder.c:2717
#: ../src/camel/camel-groupwise-folder.c:2727
#, fuzzy, c-format
msgid "Could not get message"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-folder.c:2437
#: ../src/camel/camel-groupwise-folder.c:2487
#, fuzzy, c-format
msgid "Cannot append message to folder '%s': %s"
msgstr "መልዕክቶች"

#: ../src/camel/camel-groupwise-folder.c:2473
#, fuzzy, c-format
msgid "Cannot create message: %s"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-folder.c:2653
#, c-format
msgid ""
"Cannot get message: %s\n"
"  %s"
msgstr ""

#: ../src/camel/camel-groupwise-folder.c:2653
#, fuzzy
msgid "No such message"
msgstr "ምስሉን በሌላ ስም አስቀምጥ"

#: ../src/camel/camel-groupwise-folder.c:2672
#, fuzzy, c-format
msgid "Cannot get message %s: "
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-folder.c:2690
#: ../src/camel/camel-groupwise-folder.c:2700
#: ../src/camel/camel-groupwise-folder.c:2903
#, c-format
msgid "This message is not available in offline mode."
msgstr ""

#: ../src/camel/camel-groupwise-journal.c:248
#, fuzzy, c-format
msgid "Cannot get folder container %s"
msgstr "አዲስ ዶሴ ፍጠር"

#: ../src/camel/camel-groupwise-journal.c:318
#, fuzzy, c-format
msgid "Cannot append message in offline mode: cache unavailable"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-journal.c:335
msgid "Cannot append message in offline mode: "
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:48
#, fuzzy
msgid "Checking for new mail"
msgstr "ሴኔጋል"

#: ../src/camel/camel-groupwise-provider.c:50
#, fuzzy
msgid "C_heck for new messages in all folders"
msgstr "መልዕክቶች"

#: ../src/camel/camel-groupwise-provider.c:53
msgid "Options"
msgstr "&ምርጫዎች"

#: ../src/camel/camel-groupwise-provider.c:55
msgid "_Apply filters to new messages in Inbox on this server"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:57
msgid "Check new messages for J_unk contents"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:59
msgid "Only check for Junk messages in the IN_BOX folder"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:61
msgid "Automatically synchroni_ze account locally"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:66
#, fuzzy
msgid "SOAP Settings"
msgstr "ምርጫዎች"

#: ../src/camel/camel-groupwise-provider.c:69
msgid "Post Office Agent SOAP _Port:"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:80
msgid "Default GroupWise port"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:86
#, fuzzy
msgid "Novell GroupWise"
msgstr "የውይይት መድረክ"

#: ../src/camel/camel-groupwise-provider.c:88
msgid "For accessing Novell GroupWise servers"
msgstr ""

#: ../src/camel/camel-groupwise-provider.c:105
#, fuzzy
msgid "Password"
msgstr "ሚስጢራዊ ቃል"

#: ../src/camel/camel-groupwise-provider.c:107
msgid ""
"This option will connect to the GroupWise server using a plaintext password."
msgstr ""

#: ../src/camel/camel-groupwise-store.c:141
#, fuzzy, c-format
msgid "You did not enter a password."
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-store.c:164
msgid "You must be working online to complete this operation"
msgstr ""

#: ../src/camel/camel-groupwise-store.c:263
msgid "Some features may not work correctly with your current server version"
msgstr ""

#: ../src/camel/camel-groupwise-store.c:489
#, fuzzy, c-format
msgid "No such folder %s"
msgstr "ዶሴን አስወግድ"

#: ../src/camel/camel-groupwise-store.c:1139
#, fuzzy, c-format
msgid "Cannot create GroupWise folders in offline mode."
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-store.c:1148
#, fuzzy, c-format
msgid "Cannot create a special system folder"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-store.c:1158
#, c-format
msgid "The parent folder is not allowed to contain subfolders"
msgstr ""

#: ../src/camel/camel-groupwise-store.c:1248
#: ../src/camel/camel-groupwise-store.c:1273
#, c-format
msgid "Cannot rename GroupWise folder '%s' to '%s'"
msgstr ""

#: ../src/camel/camel-groupwise-store.c:1312
#: ../src/camel/camel-groupwise-transport.c:76
#, fuzzy, c-format
msgid "GroupWise server %s"
msgstr "የውይይት መድረክ"

#: ../src/camel/camel-groupwise-store.c:1315
#, c-format
msgid "GroupWise service for %s on %s"
msgstr ""

#: ../src/camel/camel-groupwise-store.c:1523
#, fuzzy, c-format
msgid "Host or user not available in url"
msgstr "የኩነቶች መረጃ"

#: ../src/camel/camel-groupwise-transport.c:80
#, c-format
msgid "GroupWise mail delivery via %s"
msgstr ""

#: ../src/camel/camel-groupwise-transport.c:118
#, fuzzy
msgid "Sending Message"
msgstr "ምስሉን በሌላ ስም አስቀምጥ"

#: ../src/camel/camel-groupwise-transport.c:162
#, c-format
msgid ""
"You have exceeded this account's storage limit. Your messages are queued in "
"your Outbox. Resend by pressing Send/Receive after deleting/archiving some "
"of your mail.\n"
msgstr ""

#: ../src/camel/camel-groupwise-transport.c:167
#, fuzzy, c-format
msgid "Could not send message: %s"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/camel/camel-groupwise-transport.c:168
#: ../src/server/e-gw-connection.c:243
msgid "Unknown error"
msgstr "ያልታወቀ ስህተት"

#: ../src/plugins/camel-gw-listener.c:428
#: ../src/plugins/camel-gw-listener.c:569
msgid "Tasks"
msgstr ""

#: ../src/plugins/camel-gw-listener.c:429
#: ../src/plugins/camel-gw-listener.c:454
#: ../src/plugins/camel-gw-listener.c:570
#, fuzzy
msgid "Notes"
msgstr "ማስታወሻ"

#: ../src/plugins/camel-gw-listener.c:453
#, fuzzy
msgid "Checklist"
msgstr "ዝርዝር"

#. Translators: First %s is the server name, second %s is user name
#: ../src/plugins/camel-gw-listener.c:513
#, fuzzy, c-format
msgid "Enter password for %s (user %s)"
msgstr "ሚስጢራዊ ቃል አስገቡ"

#: ../src/plugins/camel-gw-listener.c:533
msgid "Failed to authenticate.\n"
msgstr ""

#: ../src/plugins/gw-ui.c:115
#, fuzzy
msgid "New _Shared Folder..."
msgstr "_አዲስ ዶሴ (_N)"

#: ../src/plugins/gw-ui.c:122
#, fuzzy
msgid "_Proxy Login..."
msgstr "አትም"

#: ../src/plugins/gw-ui.c:173
#, fuzzy
msgid "Junk Mail Settings..."
msgstr "ምርጫዎች"

#: ../src/plugins/gw-ui.c:180
#, fuzzy
msgid "Track Message Status..."
msgstr "በሌላ ስም አስቀምጥ..."

#: ../src/plugins/gw-ui.c:186
msgid "Retract Mail"
msgstr ""

#: ../src/plugins/gw-ui.c:334
msgid "Accept"
msgstr ""

#: ../src/plugins/gw-ui.c:341
#, fuzzy
msgid "Accept Tentatively"
msgstr "ትግባሮች (_A)"

#: ../src/plugins/gw-ui.c:348
msgid "Decline"
msgstr ""

#: ../src/plugins/gw-ui.c:355
#, fuzzy
msgid "Rese_nd Meeting..."
msgstr "ምርጫዎች"

#: ../src/plugins/install-shared.c:189
#: ../src/plugins/share-folder-common.c:362
#, fuzzy
msgid "Create folder"
msgstr "አዲስ ዶሴ ፍጠር"

#: ../src/plugins/install-shared.c:189
#: ../src/plugins/share-folder-common.c:362
msgid "Specify where to create the folder:"
msgstr ""

#: ../src/plugins/install-shared.c:241
#, c-format
msgid ""
"The user '%s' has shared a folder with you\n"
"\n"
"Message from '%s'\n"
"\n"
"\n"
"%s\n"
"\n"
"\n"
"Click 'Apply' to install the shared folder\n"
"\n"
msgstr ""

#: ../src/plugins/install-shared.c:253
msgid "Install the shared folder"
msgstr ""

#: ../src/plugins/install-shared.c:257
msgid "Shared Folder Installation"
msgstr ""

#: ../src/plugins/junk-mail-settings.c:82
#, fuzzy
msgid "Junk Settings"
msgstr "ምርጫዎች"

#: ../src/plugins/junk-mail-settings.c:95
#, fuzzy
msgid "Junk Mail Settings"
msgstr "ምርጫዎች"

#: ../src/plugins/junk-settings.c:408
msgid "Email"
msgstr "ኢሜይል"

#: ../src/plugins/mail-retract.c:81
#, fuzzy
msgid "Message Retract"
msgstr "መልዕክት"

#: ../src/plugins/mail-retract.c:90
msgid ""
"Retracting a message may remove it from the recipient's mailbox. Are you "
"sure you want to do this?"
msgstr ""

#: ../src/plugins/mail-retract.c:111
msgid "Message retracted successfully"
msgstr ""

#: ../src/plugins/mail-send-options.c:200
#, fuzzy
msgid "_Send Options"
msgstr "&ምርጫዎች"

#: ../src/plugins/mail-send-options.c:202
#, fuzzy
msgid "Insert Send options"
msgstr "&ምርጫዎች"

#: ../src/plugins/org-gnome-compose-send-options.xml.h:1
msgid "Add Send Options to GroupWise messages"
msgstr ""

#: ../src/plugins/org-gnome-compose-send-options.xml.h:2
#: ../src/plugins/send-options.c:212
#, fuzzy
msgid "Send Options"
msgstr "&ምርጫዎች"

#: ../src/plugins/org-gnome-groupwise-features.eplug.xml.h:1
msgid "Fine-tune your GroupWise accounts."
msgstr ""

#: ../src/plugins/org-gnome-groupwise-features.eplug.xml.h:2
#, fuzzy
msgid "GroupWise Features"
msgstr "የውይይት መድረክ"

#: ../src/plugins/org-gnome-mail-retract.error.xml.h:1
#, fuzzy
msgid "Message retract failed"
msgstr "መልዕክት"

#: ../src/plugins/org-gnome-mail-retract.error.xml.h:2
msgid "The server did not allow the selected message to be retracted."
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:1
msgid "Do you want to resend the meeting?"
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:2
msgid "Do you want to resend the recurring meeting?"
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:3
msgid "Do you want to retract the original item?"
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:4
msgid "The original will be removed from the recipient's mailbox."
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:5
msgid "This is a recurring meeting"
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:6
msgid "This will create a new meeting using the existing meeting details."
msgstr ""

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:7
msgid ""
"This will create a new meeting with the existing meeting details. The "
"recurrence rule needs to be re-entered."
msgstr ""

#. Translators: "it" is a "recurring meeting" (string refers to "This is a recurring meeting")
#: ../src/plugins/org-gnome-process-meeting.error.xml.h:9
msgid "Would you like to accept it?"
msgstr ""

#. Translators: "it" is a "recurring meeting" (string refers to "This is a recurring meeting")
#: ../src/plugins/org-gnome-process-meeting.error.xml.h:11
msgid "Would you like to decline it?"
msgstr ""

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:1
msgid "Account &quot;{0}&quot; already exists. Please check your folder tree."
msgstr ""

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:2
msgid "Account Already Exists"
msgstr ""

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:3
#: ../src/plugins/org-gnome-proxy.error.xml.h:1
#: ../src/plugins/org-gnome-shared-folder.error.xml.h:1
#, fuzzy
msgid "Invalid user"
msgstr "ጥያቄ"

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:4
msgid ""
"Proxy login as &quot;{0}&quot; was unsuccessful. Please check your email "
"address and try again."
msgstr ""

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:3
#, fuzzy
msgid "Proxy access cannot be given to user &quot;{0}&quot;"
msgstr "ፋይል sን መክፈት አልቻለም፦ %s፦ %s"

#: ../src/plugins/org-gnome-proxy.error.xml.h:4
#: ../src/plugins/org-gnome-shared-folder.error.xml.h:2
#, fuzzy
msgid "Specify User"
msgstr "ዶሴ ምረጡ"

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:6
msgid "You have already given proxy permissions to this user."
msgstr ""

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:8
msgid "You have to specify a valid user name to give proxy rights."
msgstr ""

#: ../src/plugins/org-gnome-shared-folder.error.xml.h:3
#, fuzzy
msgid "You cannot share this folder with the specified user &quot;{0}&quot;"
msgstr "ፋይል sን መክፈት አልቻለም፦ %s፦ %s"

#: ../src/plugins/org-gnome-shared-folder.error.xml.h:4
msgid "You have to specify a user name which you want to add to the list"
msgstr ""

#: ../src/plugins/proxy-login.c:208 ../src/plugins/proxy-login.c:251
#: ../src/plugins/proxy.c:493 ../src/plugins/send-options.c:84
#, c-format
msgid "%sEnter password for %s (user %s)"
msgstr ""

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:695
msgid "The Proxy tab will be available only when the account is online."
msgstr ""

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:701
msgid "The Proxy tab will be available only when the account is enabled."
msgstr ""

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:706
#, fuzzy
msgctxt "GW"
msgid "Proxy"
msgstr "ቅድሚያ"

#: ../src/plugins/proxy.c:930 ../src/plugins/share-folder.c:693
#, fuzzy
msgid "Add User"
msgstr "ጨምር"

#: ../src/plugins/send-options.c:214
#, fuzzy
msgid "Advanced send options"
msgstr "&ምርጫዎች"

#: ../src/plugins/share-folder-common.c:138
#, fuzzy, c-format
msgid "Creating folder '%s'"
msgstr "አዲስ ዶሴ ፍጠር"

#: ../src/plugins/share-folder-common.c:329 ../src/plugins/share-folder.c:728
#, fuzzy
msgid "Users"
msgstr "ተጠቃሚ፦ (_U)"

#: ../src/plugins/share-folder-common.c:330
msgid "Enter the users and set permissions"
msgstr ""

#: ../src/plugins/share-folder-common.c:427
#, fuzzy
msgid "Sharing"
msgstr "እየጀመረ ነው"

#: ../src/plugins/share-folder.c:531
#, fuzzy
msgid "Custom Notification"
msgstr ""
"ዶሴ `%s'ን ማስፈጠር አልተቻለም፦\n"
"%s"

#: ../src/plugins/share-folder.c:733
#, fuzzy
msgid "Add   "
msgstr "ጨምር"

#: ../src/plugins/share-folder.c:739
#, fuzzy
msgid "Modify"
msgstr "ለውጡ"

#: ../src/plugins/share-folder.c:745
msgid "Delete"
msgstr "አጥፉ"

#: ../src/plugins/status-track.c:125
#, fuzzy
msgid "Message Status"
msgstr "በሌላ ስም አስቀምጥ..."

#. Subject
#: ../src/plugins/status-track.c:139
#, fuzzy
msgid "Subject:"
msgstr "ጉዳዩ፦"

#: ../src/plugins/status-track.c:153
#, fuzzy
msgid "From:"
msgstr "ከ"

#: ../src/plugins/status-track.c:168
#, fuzzy
msgid "Creation date:"
msgstr "አውቶማቲክ"

#: ../src/plugins/status-track.c:208
#, fuzzy
msgid "Recipient: "
msgstr "ደቂቃዎች"

#: ../src/plugins/status-track.c:215
#, fuzzy
msgid "Delivered: "
msgstr "አጥፉ"

#: ../src/plugins/status-track.c:221
#, fuzzy
msgid "Opened: "
msgstr "ክፈት"

#: ../src/plugins/status-track.c:226
msgid "Accepted: "
msgstr ""

#: ../src/plugins/status-track.c:231
#, fuzzy
msgid "Deleted: "
msgstr "የጠፋ"

#: ../src/plugins/status-track.c:236
msgid "Declined: "
msgstr ""

#: ../src/plugins/status-track.c:241
#, fuzzy
msgid "Completed: "
msgstr "አውቶማቲክ"

#: ../src/plugins/status-track.c:246
#, fuzzy
msgid "Undelivered: "
msgstr "አጥፉ"

#: ../src/server/e-gw-connection.c:227
#, fuzzy
msgid "Invalid connection"
msgstr "%d ሴኮንዶች"

#: ../src/server/e-gw-connection.c:229
#, fuzzy
msgid "Invalid object"
msgstr "%d ሴኮንዶች"

#: ../src/server/e-gw-connection.c:231
#, fuzzy
msgid "Invalid response from server"
msgstr "ጥያቄ"

#: ../src/server/e-gw-connection.c:233
msgid "No response from the server"
msgstr ""

#: ../src/server/e-gw-connection.c:235
#, fuzzy
msgid "Object not found"
msgstr "አልተገኘም"

#: ../src/server/e-gw-connection.c:237
#, fuzzy
msgid "Unknown User"
msgstr "ያልታወቀ ስህተት"

#: ../src/server/e-gw-connection.c:239
msgid "Bad parameter"
msgstr ""
