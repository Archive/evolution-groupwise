# Asturian translation for evolution
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the evolution package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: evolution\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-05-11 14:35+0530\n"
"PO-Revision-Date: 2011-02-13 19:42+0100\n"
"Last-Translator: ivarela <ivarela@softastur.org>\n"
"Language-Team: Asturian <ast@li.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2008-10-22 09:49+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: ../src/addressbook/e-book-backend-groupwise.c:2261
#: ../src/addressbook/e-book-backend-groupwise.c:2283
#: ../src/addressbook/e-book-backend-groupwise.c:2328
#, fuzzy
msgid "Searching..."
msgstr "Gueta"

#: ../src/addressbook/e-book-backend-groupwise.c:2330
msgid "Loading..."
msgstr "Cargando..."

#: ../src/addressbook/e-book-backend-groupwise.c:2699
#, c-format
msgid "Downloading contacts (%d)... "
msgstr "Descargando contautos (%d)... "

#: ../src/addressbook/e-book-backend-groupwise.c:2849
#: ../src/addressbook/e-book-backend-groupwise.c:3030
#: ../src/addressbook/e-book-backend-groupwise.c:3077
#, c-format
msgid "Updating contacts cache (%d)... "
msgstr "Anovando caché de contautos (%d)… "

#: ../src/calendar/e-cal-backend-groupwise-utils.c:1130
#: ../src/camel/camel-groupwise-folder.c:2114
#, fuzzy
msgid "Reply Requested: by "
msgstr "_Rempuesta solicitada"

#: ../src/calendar/e-cal-backend-groupwise-utils.c:1135
#: ../src/camel/camel-groupwise-folder.c:2119
#, fuzzy
msgid "Reply Requested: When convenient"
msgstr "_Cuandu convenga"

#: ../src/calendar/e-cal-backend-groupwise.c:329
#, fuzzy
msgid "Loading Appointment items"
msgstr "Cita - %s"

#: ../src/calendar/e-cal-backend-groupwise.c:331
#, fuzzy
msgid "Loading Task items"
msgstr "Cargando xeres"

#: ../src/calendar/e-cal-backend-groupwise.c:333
#, fuzzy
msgid "Loading Note items"
msgstr "Cargando notes"

#: ../src/calendar/e-cal-backend-groupwise.c:335
#, fuzzy
msgid "Loading items"
msgstr "Cargando notes"

#: ../src/calendar/e-cal-backend-groupwise.c:951
#: ../src/plugins/camel-gw-listener.c:427
#: ../src/plugins/camel-gw-listener.c:452
#: ../src/plugins/camel-gw-listener.c:568
msgid "Calendar"
msgstr "Calendariu"

#: ../src/calendar/e-cal-backend-groupwise.c:1014
#, fuzzy
msgid "Invalid server URI"
msgstr "Versión del sirvidor inválida"

#: ../src/calendar/e-cal-backend-groupwise.c:1115
#: ../src/calendar/e-cal-backend-groupwise.c:1361
#, fuzzy
msgid "Could not create cache file"
msgstr "Nun ye dable crear un mensax."

#: ../src/calendar/e-cal-backend-groupwise.c:1127
#, fuzzy
msgid "Could not create thread for populating cache"
msgstr "Nun se pudo asoleyar el filu d'asoleyamientu."

#: ../src/camel/camel-groupwise-folder.c:958
#, fuzzy, c-format
msgid "Could not load summary for %s"
msgstr "Nun se pudo cargar «%s»"

#: ../src/camel/camel-groupwise-folder.c:1054
#, fuzzy, c-format
msgid "Checking for deleted messages %s"
msgstr "Comprebando si hai mensaxes nuevos"

#: ../src/camel/camel-groupwise-folder.c:1314
#: ../src/camel/camel-groupwise-folder.c:1332
#: ../src/camel/camel-groupwise-store.c:566
#: ../src/camel/camel-groupwise-store.c:748
#: ../src/camel/camel-groupwise-transport.c:112
#: ../src/camel/camel-groupwise-transport.c:128
#, fuzzy, c-format
msgid "Authentication failed"
msgstr "Falló n'autenticación"

#: ../src/camel/camel-groupwise-folder.c:1530
#: ../src/camel/camel-groupwise-store.c:603
#: ../src/camel/camel-groupwise-store.c:734
#, fuzzy, c-format
msgid "Fetching summary information for new messages in %s"
msgstr "Obteniendo la información de la cuota pa la carpeta «%s»"

#: ../src/camel/camel-groupwise-folder.c:2000
#: ../src/camel/camel-groupwise-folder.c:2053
#: ../src/camel/camel-groupwise-folder.c:2717
#: ../src/camel/camel-groupwise-folder.c:2727
#, fuzzy, c-format
msgid "Could not get message"
msgstr "Nun ye dable crear un mensax."

#: ../src/camel/camel-groupwise-folder.c:2437
#: ../src/camel/camel-groupwise-folder.c:2487
#, fuzzy, c-format
msgid "Cannot append message to folder '%s': %s"
msgstr "Guardando mensaxe na carpeta «%s»"

#: ../src/camel/camel-groupwise-folder.c:2473
#, fuzzy, c-format
msgid "Cannot create message: %s"
msgstr "Nun ye dable crear un mensax."

#: ../src/camel/camel-groupwise-folder.c:2653
#, fuzzy, c-format
msgid ""
"Cannot get message: %s\n"
"  %s"
msgstr "Nun se pudo obtener la llista d'orixe. %s"

#: ../src/camel/camel-groupwise-folder.c:2653
#, fuzzy
msgid "No such message"
msgstr "Entrugar por cada mensax"

#: ../src/camel/camel-groupwise-folder.c:2672
#, fuzzy, c-format
msgid "Cannot get message %s: "
msgstr "Nun se pudo obtener la llista d'orixe. %s"

#: ../src/camel/camel-groupwise-folder.c:2690
#: ../src/camel/camel-groupwise-folder.c:2700
#: ../src/camel/camel-groupwise-folder.c:2903
#, fuzzy, c-format
msgid "This message is not available in offline mode."
msgstr "Nun ta disponible en mou desconeutáu"

#: ../src/camel/camel-groupwise-journal.c:248
#, fuzzy, c-format
msgid "Cannot get folder container %s"
msgstr "Nun se pudo obtener la llista d'orixe. %s"

#: ../src/camel/camel-groupwise-journal.c:318
#, c-format
msgid "Cannot append message in offline mode: cache unavailable"
msgstr ""
"Nun puede amestase'l mensax en mou ensin conexón: la caché nun ta disponible"

#: ../src/camel/camel-groupwise-journal.c:335
#, fuzzy
msgid "Cannot append message in offline mode: "
msgstr "Nun ta disponible en mou desconeutáu"

#: ../src/camel/camel-groupwise-provider.c:48
msgid "Checking for new mail"
msgstr "Comprobando corréu nuevu"

#: ../src/camel/camel-groupwise-provider.c:50
#, fuzzy
msgid "C_heck for new messages in all folders"
msgstr "Comprobar si hai corréu _nuevu cada"

#: ../src/camel/camel-groupwise-provider.c:53
msgid "Options"
msgstr "Opciones"

#: ../src/camel/camel-groupwise-provider.c:55
#, fuzzy
msgid "_Apply filters to new messages in Inbox on this server"
msgstr ""
"Conseña si hai de notificar namaí los mensaxes na carpeta bandexa d'entrada."

#: ../src/camel/camel-groupwise-provider.c:57
#, fuzzy
msgid "Check new messages for J_unk contents"
msgstr "Comprobar si hai corréu _nuevu cada"

#: ../src/camel/camel-groupwise-provider.c:59
#, fuzzy
msgid "Only check for Junk messages in the IN_BOX folder"
msgstr "Nun hai mensaxes nesta carpeta"

#: ../src/camel/camel-groupwise-provider.c:61
msgid "Automatically synchroni_ze account locally"
msgstr "_Sincronizar automáticamente la cuenta llocalmente"

#: ../src/camel/camel-groupwise-provider.c:66
#, fuzzy
msgid "SOAP Settings"
msgstr "Preferencies"

#: ../src/camel/camel-groupwise-provider.c:69
msgid "Post Office Agent SOAP _Port:"
msgstr "_Puertu del axente de la oficina de Correos SOAP:"

#: ../src/camel/camel-groupwise-provider.c:80
#, fuzzy
msgid "Default GroupWise port"
msgstr "Prioridá predeterminada:"

#: ../src/camel/camel-groupwise-provider.c:86
#, fuzzy
msgid "Novell GroupWise"
msgstr "GroupWise"

#: ../src/camel/camel-groupwise-provider.c:88
#, fuzzy
msgid "For accessing Novell GroupWise servers"
msgstr "Nun ye dable coneutase col servidor GroupWise."

#: ../src/camel/camel-groupwise-provider.c:105
#, fuzzy
msgid "Password"
msgstr "Contraseña:"

#: ../src/camel/camel-groupwise-provider.c:107
#, fuzzy
msgid ""
"This option will connect to the GroupWise server using a plaintext password."
msgstr "Nun ye dable coneutase col servidor GroupWise."

#: ../src/camel/camel-groupwise-store.c:141
#, fuzzy, c-format
msgid "You did not enter a password."
msgstr "Nun ye dable crear un mensax."

#: ../src/camel/camel-groupwise-store.c:164
msgid "You must be working online to complete this operation"
msgstr ""
"Pa completar esta operación tienes que tar trabayando coneutáu a la rede"

#: ../src/camel/camel-groupwise-store.c:263
#, fuzzy
msgid "Some features may not work correctly with your current server version"
msgstr ""
"Delles carauterístiques seique nun furrulen afayadizamente col to sirvidor "
"actual"

#: ../src/camel/camel-groupwise-store.c:489
#, fuzzy, c-format
msgid "No such folder %s"
msgstr "Moviendo la carpeta %s"

#: ../src/camel/camel-groupwise-store.c:1139
#, c-format
msgid "Cannot create GroupWise folders in offline mode."
msgstr "Nun pueden crease les carpetes GroupWise en mou desconeutáu."

#: ../src/camel/camel-groupwise-store.c:1148
#, fuzzy, c-format
msgid "Cannot create a special system folder"
msgstr "Nun pue renomase o movese la carpeta del sistema \"{0}\"."

#: ../src/camel/camel-groupwise-store.c:1158
#, fuzzy, c-format
msgid "The parent folder is not allowed to contain subfolders"
msgstr ""
"¿Tas seguru de que quies desaniciar la carpeta «{0}» y toles sos subcarpetes?"

#: ../src/camel/camel-groupwise-store.c:1248
#: ../src/camel/camel-groupwise-store.c:1273
#, fuzzy, c-format
msgid "Cannot rename GroupWise folder '%s' to '%s'"
msgstr "Nun se pue crear e mensaxe soup pa la URL «%s»"

#: ../src/camel/camel-groupwise-store.c:1312
#: ../src/camel/camel-groupwise-transport.c:76
#, fuzzy, c-format
msgid "GroupWise server %s"
msgstr "Característiques de GroupWise"

#: ../src/camel/camel-groupwise-store.c:1315
#, c-format
msgid "GroupWise service for %s on %s"
msgstr "Serviciu GroupWise pa %s en %s"

#: ../src/camel/camel-groupwise-store.c:1523
#, fuzzy, c-format
msgid "Host or user not available in url"
msgstr "Nun hai un editor de categoríes disponible"

#: ../src/camel/camel-groupwise-transport.c:80
#, c-format
msgid "GroupWise mail delivery via %s"
msgstr "Entrega de corréu Groupwise per aciu de %s"

#: ../src/camel/camel-groupwise-transport.c:118
#, fuzzy
msgid "Sending Message"
msgstr "Unviando mensax"

#: ../src/camel/camel-groupwise-transport.c:162
#, c-format
msgid ""
"You have exceeded this account's storage limit. Your messages are queued in "
"your Outbox. Resend by pressing Send/Receive after deleting/archiving some "
"of your mail.\n"
msgstr ""
"Sobrepasasti el llímite d'atroxamientu d'esta cuenta. Los mensaxes encólense "
"na to Bandexa de salida. Reunvíalos calcando Unviar/Recibir dempués de "
"desaniciar/archivar  dalgunos de los mensaxes.\n"

#: ../src/camel/camel-groupwise-transport.c:167
#, fuzzy, c-format
msgid "Could not send message: %s"
msgstr "Nun se pudo analizar el mensaxe PGP: "

#: ../src/camel/camel-groupwise-transport.c:168
#: ../src/server/e-gw-connection.c:243
msgid "Unknown error"
msgstr "Fallu desconocíu"

#: ../src/plugins/camel-gw-listener.c:428
#: ../src/plugins/camel-gw-listener.c:569
msgid "Tasks"
msgstr "Xeres"

#: ../src/plugins/camel-gw-listener.c:429
#: ../src/plugins/camel-gw-listener.c:454
#: ../src/plugins/camel-gw-listener.c:570
msgid "Notes"
msgstr "Notes"

#: ../src/plugins/camel-gw-listener.c:453
msgid "Checklist"
msgstr "Llista de comprebación"

#. Translators: First %s is the server name, second %s is user name
#: ../src/plugins/camel-gw-listener.c:513
#, c-format
msgid "Enter password for %s (user %s)"
msgstr "Introduz la contraseña pa %s (usuariu %s)"

#: ../src/plugins/camel-gw-listener.c:533
msgid "Failed to authenticate.\n"
msgstr "Nun ye dable autenticar.\n"

#: ../src/plugins/gw-ui.c:115
msgid "New _Shared Folder..."
msgstr "Carpeta compartida _nueva…"

#: ../src/plugins/gw-ui.c:122
msgid "_Proxy Login..."
msgstr "Sesión _proxy…"

#: ../src/plugins/gw-ui.c:173
msgid "Junk Mail Settings..."
msgstr "Opciones de SPAM…"

#: ../src/plugins/gw-ui.c:180
msgid "Track Message Status..."
msgstr "Siguir l'estáu del mensax…"

#: ../src/plugins/gw-ui.c:186
msgid "Retract Mail"
msgstr "Retractar corréu"

#: ../src/plugins/gw-ui.c:334
msgid "Accept"
msgstr "Aceutar"

#: ../src/plugins/gw-ui.c:341
msgid "Accept Tentatively"
msgstr "Aceutar provisionalmente"

#: ../src/plugins/gw-ui.c:348
msgid "Decline"
msgstr "Refugar"

#: ../src/plugins/gw-ui.c:355
msgid "Rese_nd Meeting..."
msgstr "Reun_viar reunión…"

#: ../src/plugins/install-shared.c:189
#: ../src/plugins/share-folder-common.c:362
msgid "Create folder"
msgstr "Crear carpeta"

#: ../src/plugins/install-shared.c:189
#: ../src/plugins/share-folder-common.c:362
msgid "Specify where to create the folder:"
msgstr "Especifica aú crear la carpeta:"

#: ../src/plugins/install-shared.c:241
#, c-format
msgid ""
"The user '%s' has shared a folder with you\n"
"\n"
"Message from '%s'\n"
"\n"
"\n"
"%s\n"
"\n"
"\n"
"Click 'Apply' to install the shared folder\n"
"\n"
msgstr ""
"L'usuariu «%s» compartió una carpeta contigo\n"
"\n"
"Mensaxe de «%s»\n"
"\n"
"\n"
"%s\n"
"\n"
"\n"
"Calca «Aplicar» pa instalar la carpeta compartida\n"
"\n"

#: ../src/plugins/install-shared.c:253
msgid "Install the shared folder"
msgstr "Instalar la carpeta compartida"

#: ../src/plugins/install-shared.c:257
msgid "Shared Folder Installation"
msgstr "Instalación de carpeta compartida"

#: ../src/plugins/junk-mail-settings.c:82
msgid "Junk Settings"
msgstr "Opciones de Corréu puxarra (SPAM)"

#: ../src/plugins/junk-mail-settings.c:95
msgid "Junk Mail Settings"
msgstr "Configuración de Corréu puxarra (SPAM)"

#: ../src/plugins/junk-settings.c:408
msgid "Email"
msgstr "Corréu-e"

#: ../src/plugins/mail-retract.c:81
msgid "Message Retract"
msgstr "Retirada de mensax"

#: ../src/plugins/mail-retract.c:90
msgid ""
"Retracting a message may remove it from the recipient's mailbox. Are you "
"sure you want to do this?"
msgstr ""
"Retirar un corréu puede desanicialu del buzón de corréu del destinatariu. "
"¿Tas seguru de que quies facer eso?"

#: ../src/plugins/mail-retract.c:111
msgid "Message retracted successfully"
msgstr "Mensax retractáu con éxitu"

#: ../src/plugins/mail-send-options.c:200
msgid "_Send Options"
msgstr "Opciones d'_unvíu"

#: ../src/plugins/mail-send-options.c:202
msgid "Insert Send options"
msgstr "Enxertar opciones d'unvíu"

#: ../src/plugins/org-gnome-compose-send-options.xml.h:1
msgid "Add Send Options to GroupWise messages"
msgstr "Amestar opciones d'unvíu a los mensaxes de GroupWise"

#: ../src/plugins/org-gnome-compose-send-options.xml.h:2
#: ../src/plugins/send-options.c:212
msgid "Send Options"
msgstr "Opciones d'Unvíu"

#: ../src/plugins/org-gnome-groupwise-features.eplug.xml.h:1
msgid "Fine-tune your GroupWise accounts."
msgstr "Configurar les cuentes GroupWise."

#: ../src/plugins/org-gnome-groupwise-features.eplug.xml.h:2
msgid "GroupWise Features"
msgstr "Característiques de GroupWise"

#: ../src/plugins/org-gnome-mail-retract.error.xml.h:1
msgid "Message retract failed"
msgstr "Falló al retirar el mensax"

#: ../src/plugins/org-gnome-mail-retract.error.xml.h:2
msgid "The server did not allow the selected message to be retracted."
msgstr "El sirvidor nun dexó que se retirara'l mensax escoyíu."

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:1
msgid "Do you want to resend the meeting?"
msgstr "¿Quies reunviar la reunión?"

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:2
msgid "Do you want to resend the recurring meeting?"
msgstr "¿Quies reunviar la reunión repetitiva?"

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:3
msgid "Do you want to retract the original item?"
msgstr "¿Quies retirar l'elementu orixinal?"

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:4
msgid "The original will be removed from the recipient's mailbox."
msgstr "L'orixinal quitaráse del buzón d'entrada del destinatariu."

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:5
msgid "This is a recurring meeting"
msgstr "Esta ye una cita repetitiva"

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:6
msgid "This will create a new meeting using the existing meeting details."
msgstr ""
"Esto fadrá un mensax nuevu usando los detalles esistentes de la reunión."

#: ../src/plugins/org-gnome-process-meeting.error.xml.h:7
msgid ""
"This will create a new meeting with the existing meeting details. The "
"recurrence rule needs to be re-entered."
msgstr ""
"Esto fadrá una nueva reunión colos detalles esistentes de la reunión. Tien "
"d'introducise la regla de repetición."

#. Translators: "it" is a "recurring meeting" (string refers to "This is a recurring meeting")
#: ../src/plugins/org-gnome-process-meeting.error.xml.h:9
msgid "Would you like to accept it?"
msgstr "¿Quies aceptalo?"

#. Translators: "it" is a "recurring meeting" (string refers to "This is a recurring meeting")
#: ../src/plugins/org-gnome-process-meeting.error.xml.h:11
msgid "Would you like to decline it?"
msgstr "¿Quies refugalo?"

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:1
msgid "Account &quot;{0}&quot; already exists. Please check your folder tree."
msgstr ""
"La cuenta &quot;{0}&quot; yá existe. Mira a ver el to árbol de carpetes."

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:2
msgid "Account Already Exists"
msgstr "La cuenta yá existe"

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:3
#: ../src/plugins/org-gnome-proxy.error.xml.h:1
#: ../src/plugins/org-gnome-shared-folder.error.xml.h:1
msgid "Invalid user"
msgstr "Usuariu non válidu"

#: ../src/plugins/org-gnome-proxy-login.error.xml.h:4
msgid ""
"Proxy login as &quot;{0}&quot; was unsuccessful. Please check your email "
"address and try again."
msgstr ""
"L'entamu de sesion como &quot;{0}&quot; nel proxy falló. Mira a ver la to "
"direición de corréu-e yá inténtalo otra vuelta."

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:3
msgid "Proxy access cannot be given to user &quot;{0}&quot;"
msgstr "Nun se puede dar accesu al proxy al usuariu &quot;{0}&quot;"

#: ../src/plugins/org-gnome-proxy.error.xml.h:4
#: ../src/plugins/org-gnome-shared-folder.error.xml.h:2
msgid "Specify User"
msgstr "Especificar usuariu"

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:6
msgid "You have already given proxy permissions to this user."
msgstr "Yá diste permisos al proxy a esti usuariu."

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/org-gnome-proxy.error.xml.h:8
msgid "You have to specify a valid user name to give proxy rights."
msgstr ""
"Tienes qu'especificar un nome d'usuariu válidu pa da-y permisos nel proxy."

#: ../src/plugins/org-gnome-shared-folder.error.xml.h:3
msgid "You cannot share this folder with the specified user &quot;{0}&quot;"
msgstr ""
"Non puede compartise la carpeta col usuariu &quot;{0}&quot; especificáu"

#: ../src/plugins/org-gnome-shared-folder.error.xml.h:4
msgid "You have to specify a user name which you want to add to the list"
msgstr "Tienes d'especificar el nome d'usuariu que quies amestar a la llista"

#: ../src/plugins/proxy-login.c:208 ../src/plugins/proxy-login.c:251
#: ../src/plugins/proxy.c:493 ../src/plugins/send-options.c:84
#, c-format
msgid "%sEnter password for %s (user %s)"
msgstr "%s Introduz la contraseña pa %s (usuariu %s)"

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:695
msgid "The Proxy tab will be available only when the account is online."
msgstr ""
"La llingüeta del Proxy tará disponible namái cuando la cuenta tea coneutada."

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:701
msgid "The Proxy tab will be available only when the account is enabled."
msgstr ""
"La llingüeta del Proxy tará disponible namái cuando la cuenta tea activada"

#. To Translators: In this case, Proxy does not mean something like 'HTTP Proxy', but a GroupWise feature by which one person can send/read mails/appointments using another person's identity without knowing his password, for example if that other person is on vacation
#: ../src/plugins/proxy.c:706
msgctxt "GW"
msgid "Proxy"
msgstr "Apoderáu"

#: ../src/plugins/proxy.c:930 ../src/plugins/share-folder.c:693
msgid "Add User"
msgstr "Amestar usuariu"

#: ../src/plugins/send-options.c:214
msgid "Advanced send options"
msgstr "Opciones d'unvíu avanzaes"

#: ../src/plugins/share-folder-common.c:138
#, c-format
msgid "Creating folder '%s'"
msgstr "Creando la carpeta «%s»"

#: ../src/plugins/share-folder-common.c:329 ../src/plugins/share-folder.c:728
msgid "Users"
msgstr "Usuarios"

#: ../src/plugins/share-folder-common.c:330
msgid "Enter the users and set permissions"
msgstr "Introduz los usuarios y afita permisos"

#: ../src/plugins/share-folder-common.c:427
msgid "Sharing"
msgstr "Compartir"

#: ../src/plugins/share-folder.c:531
msgid "Custom Notification"
msgstr "Notificación personalizada"

#: ../src/plugins/share-folder.c:733
msgid "Add   "
msgstr "Amestar   "

#: ../src/plugins/share-folder.c:739
msgid "Modify"
msgstr "Camudar"

#: ../src/plugins/share-folder.c:745
msgid "Delete"
msgstr "Esaniciar"

#: ../src/plugins/status-track.c:125
msgid "Message Status"
msgstr "Estáu del mensax"

#. Subject
#: ../src/plugins/status-track.c:139
msgid "Subject:"
msgstr "Asuntu:"

#: ../src/plugins/status-track.c:153
msgid "From:"
msgstr "De:"

#: ../src/plugins/status-track.c:168
msgid "Creation date:"
msgstr "Data de criación:"

#: ../src/plugins/status-track.c:208
msgid "Recipient: "
msgstr "Destinatariu: "

#: ../src/plugins/status-track.c:215
msgid "Delivered: "
msgstr "Entregáu: "

#: ../src/plugins/status-track.c:221
msgid "Opened: "
msgstr "Abiertu: "

#: ../src/plugins/status-track.c:226
msgid "Accepted: "
msgstr "Aceutáu: "

#: ../src/plugins/status-track.c:231
msgid "Deleted: "
msgstr "Desaniciáu: "

#: ../src/plugins/status-track.c:236
msgid "Declined: "
msgstr "Refugáu: "

#: ../src/plugins/status-track.c:241
msgid "Completed: "
msgstr "Completáu: "

#: ../src/plugins/status-track.c:246
msgid "Undelivered: "
msgstr "Ensin entregar: "

#: ../src/server/e-gw-connection.c:227
#, fuzzy
msgid "Invalid connection"
msgstr "Contautu inválidu."

#: ../src/server/e-gw-connection.c:229
msgid "Invalid object"
msgstr "Oxetu inválidu"

#: ../src/server/e-gw-connection.c:231
#, fuzzy
msgid "Invalid response from server"
msgstr "Ensin respuesta del sirvidor"

#: ../src/server/e-gw-connection.c:233
#, fuzzy
msgid "No response from the server"
msgstr "Ensin respuesta del sirvidor"

#: ../src/server/e-gw-connection.c:235
#, fuzzy
msgid "Object not found"
msgstr "Contautu non alcontráu"

#: ../src/server/e-gw-connection.c:237
#, fuzzy
msgid "Unknown User"
msgstr "Fallu desconocíu"

#: ../src/server/e-gw-connection.c:239
msgid "Bad parameter"
msgstr "Parámetru inválidu"
