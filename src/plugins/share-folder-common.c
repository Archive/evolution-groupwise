/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 *
 * Authors:
 *		Vivek Jain <jvivek@novell.com>
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>
#include <e-util/e-config.h>
#include <libemail-utils/e-account-utils.h>
#include <libemail-utils/mail-mt.h>
#include <mail/e-mail-backend.h>
#include <mail/em-config.h>
#include <mail/em-folder-properties.h>
#include <mail/em-folder-tree.h>
#include <mail/em-folder-tree-model.h>
#include <mail/em-folder-selector.h>
#include <mail/mail-vfolder-ui.h>
#include <mail/em-utils.h>
#include <mail/em-vfolder-editor-rule.h>
#include <filter/e-filter-rule.h>
#include <e-gw-container.h>
#include <e-gw-connection.h>
#include <shell/e-shell.h>
#include <shell/e-shell-sidebar.h>
#include "share-folder.h"
#include "gw-ui.h"

#define d(x)

ShareFolder *common = NULL;
CamelSession *session;
struct ShareInfo {
	GtkWidget *d;
	ShareFolder *sf;
	EMFolderTreeModel *model;
	EMFolderSelector *emfs;
};

GtkWidget * org_gnome_shared_folder_factory (EPlugin *ep, EConfigHookItemFactoryData *hook_data);
void shared_folder_commit (EPlugin *ep, EConfigTarget *tget);
void shared_folder_abort (EPlugin *ep, EConfigTarget *target);

static void refresh_folder_tree (EMFolderTreeModel *model, CamelStore *store);

static void
refresh_folder_tree (EMFolderTreeModel *model,
                     CamelStore *store)
{
	EAccount *account;
	const gchar *uid;

	uid = camel_service_get_uid (CAMEL_SERVICE (store));
	account = e_get_account_by_uid (uid);

	if (account == NULL)
		return;

	em_folder_tree_model_remove_store (model, store);

	em_folder_tree_model_add_store (model, store);
}

void
shared_folder_commit (EPlugin *ep,
                      EConfigTarget *tget)
{
	EMConfigTargetFolder *target =  (EMConfigTargetFolder *) tget->config->target;
	CamelStore *parent_store;
	EMFolderTreeModel *model;
	EShell *shell;
	EShellBackend *shell_backend;
	EMailBackend *backend;
	EMailSession *session;

	shell = e_shell_get_default ();
	shell_backend = e_shell_get_backend_by_name (shell, "mail");

	backend = E_MAIL_BACKEND (shell_backend);
	session = e_mail_backend_get_session (backend);

	model = em_folder_tree_model_new ();
	em_folder_tree_model_set_session (model, session);

	parent_store = camel_folder_get_parent_store (target->folder);

	if (common) {
		share_folder (common);
		refresh_folder_tree (model, parent_store);
		g_object_run_dispose ((GObject *) common);
		common = NULL;
	}
}

void
shared_folder_abort (EPlugin *ep,
                     EConfigTarget *target)
{
	if (common) {
		g_object_run_dispose ((GObject *) common);
		common = NULL;
	}
}

struct _EMCreateFolder {
	MailMsg base;

	/* input data */
	CamelStore *store;
	gchar *full_name;
	gchar *parent;
	gchar *name;

	/* output data */
	CamelFolderInfo *fi;

	/* callback data */
	void (* done) (struct _EMCreateFolder *m, gpointer user_data);
	gpointer user_data;
};

static gchar *
create_folder_desc (struct _EMCreateFolder *m)
{
	return g_strdup_printf (_("Creating folder '%s'"), m->full_name);
}

static void
create_folder_exec (struct _EMCreateFolder *m,
                    GCancellable *cancellable,
                    GError **error)
{
	d(printf ("creating folder parent='%s' name='%s' full_name='%s'\n", m->parent, m->name, m->full_name));

	if ((m->fi = camel_store_create_folder_sync (
		m->store, m->parent, m->name, cancellable, error))) {

		if (CAMEL_IS_SUBSCRIBABLE (m->store))
			camel_subscribable_subscribe_folder_sync (
				CAMEL_SUBSCRIBABLE (m->store),
				m->full_name, cancellable, error);
	}
}

static void
create_folder_done (struct _EMCreateFolder *m)
{
	struct ShareInfo *ssi = (struct ShareInfo *) m->user_data;
	CamelStore *store = CAMEL_STORE (m->store);
	EGwConnection *ccnc;

	if (m->done) {
		ccnc = get_cnc (store);
		if (E_IS_GW_CONNECTION (ccnc)) {
			(ssi->sf)->cnc = ccnc;

			(ssi->sf)->container_id = g_strdup (get_container_id ((ssi->sf)->cnc, m->full_name));
			share_folder (ssi->sf);
		}

		m->done (m, m->user_data);
	}
}

static void
create_folder_free (struct _EMCreateFolder *m)
{
	camel_store_free_folder_info (m->store, m->fi);
	g_object_unref (m->store);
	g_free (m->full_name);
	g_free (m->parent);
	g_free (m->name);
}

static MailMsgInfo create_folder_info = {
	sizeof (struct _EMCreateFolder),
	(MailMsgDescFunc) create_folder_desc,
	(MailMsgExecFunc) create_folder_exec,
	(MailMsgDoneFunc) create_folder_done,
	(MailMsgFreeFunc) create_folder_free
};

static void
new_folder_created_cb (struct _EMCreateFolder *m,
                       gpointer user_data)
{
	struct ShareInfo *ssi = (struct ShareInfo *) user_data;
	EMFolderSelector *emfs = ssi->emfs;
	if (m->fi) {
		refresh_folder_tree (ssi->model, m->store);
		gtk_widget_destroy ((GtkWidget *) emfs);
		gtk_widget_destroy ((GtkWidget *) ssi->d);
	}

	g_object_unref (emfs);
}

static gint
create_folder (CamelStore *store,
               const gchar *full_name,
               void (* done) (struct _EMCreateFolder *m,
               gpointer user_data),
               gpointer user_data)
{
	gchar *name, *namebuf = NULL;
	struct _EMCreateFolder *m;
	const gchar *parent;
	gint id;

	namebuf = g_strdup (full_name);
	if (!(name = strrchr (namebuf, '/'))) {
		name = namebuf;
		parent = "";
	} else {
		*name++ = '\0';
		parent = namebuf;
	}

	m = mail_msg_new (&create_folder_info);
	g_object_ref (store);
	m->store = store;
	m->full_name = g_strdup (full_name);
	m->parent = g_strdup (parent);
	m->name = g_strdup (name);
	m->user_data = (struct ShareInfo *) user_data;
	m->done = done;
	g_free (namebuf);
	id = m->base.seq;
	mail_msg_unordered_push (m);

	return id;
}

static void
users_dialog_response (GtkWidget *dialog,
                       gint response,
                       struct ShareInfo *ssi)
{
	struct _EMFolderTreeModelStoreInfo *si;
	EMFolderSelector *emfs = ssi->emfs;
	EMFolderTree *folder_tree;
	EMailSession *session;
	CamelStore *store;
	gchar *folder_name;

	if (response != GTK_RESPONSE_OK) {
		gtk_widget_destroy ((GtkWidget *) emfs);
		gtk_widget_destroy (dialog);
		return;
	}

	folder_tree = em_folder_selector_get_folder_tree (emfs);
	session = em_folder_tree_get_session (folder_tree);

	if (!em_folder_tree_get_selected (folder_tree, &store, &folder_name))
		g_return_if_reached ();

	si = em_folder_tree_model_lookup_store_info (ssi->model, store);
	if (si == NULL) {
		g_assert_not_reached ();
		return;
	}

	if (CAMEL_IS_VEE_STORE (store)) {
		EFilterRule *rule;

		rule = em_vfolder_editor_rule_new (session);
		e_filter_rule_set_name (rule, folder_name);
		vfolder_gui_add_rule (EM_VFOLDER_RULE (rule));
		gtk_widget_destroy ((GtkWidget *) emfs);
	} else {
		g_object_ref (emfs);
		ssi->d = dialog;
		create_folder (
			si->store, folder_name, new_folder_created_cb, ssi);
	}

	g_object_unref (store);
	g_free (folder_name);
}

static void
new_folder_response (EMFolderSelector *emfs,
                     gint response,
                     EMFolderTreeModel *model)
{
	GtkWidget *users_dialog;
	GtkWidget *content_area;
	GtkWidget *w;
	struct ShareInfo *ssi;
	const gchar *uri;
	EGwConnection *cnc;
	CamelService *service = NULL;
	CamelURL *url;

	ssi = g_new0 (struct ShareInfo, 1);
	if (response != GTK_RESPONSE_OK) {
		gtk_widget_destroy ((GtkWidget *) emfs);
		return;
	}

	/* i want store at this point to get cnc not sure proper or not*/
	uri = em_folder_selector_get_selected_uri (emfs);

	url = camel_url_new (uri, NULL);
	if (url != NULL) {
		service = camel_session_get_service_by_url (
			session, url, CAMEL_PROVIDER_STORE);
		camel_url_free (url);
	}

	if (!CAMEL_IS_STORE (service))
		return;

	cnc = get_cnc (CAMEL_STORE (service));
	users_dialog = gtk_dialog_new_with_buttons (
			_("Users"), NULL, GTK_DIALOG_DESTROY_WITH_PARENT, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,GTK_STOCK_OK, GTK_RESPONSE_OK, NULL);
	w = gtk_label_new_with_mnemonic (_("Enter the users and set permissions"));
	gtk_widget_show (w);
	content_area = gtk_dialog_get_content_area (GTK_DIALOG (users_dialog));
	gtk_box_pack_start (GTK_BOX (content_area), (GtkWidget *) w, TRUE, TRUE, 6);
	ssi->sf = share_folder_new (cnc, NULL);
	gtk_widget_set_sensitive (GTK_WIDGET ((ssi->sf)->table), TRUE);
	ssi->model = model;
	ssi->emfs = emfs;
	gtk_widget_reparent (GTK_WIDGET ((ssi->sf)->table), content_area);
	gtk_widget_hide ((GtkWidget *) emfs);
	gtk_window_resize (GTK_WINDOW (users_dialog), 350, 300);
	gtk_widget_show (users_dialog);
	g_signal_connect (users_dialog, "response", G_CALLBACK (users_dialog_response), ssi);
}

void
gw_new_shared_folder_cb (GtkAction *action,
                         EShellView *shell_view)
{
	EMFolderSelector *selector;
	EMFolderTree *folder_tree;
	GtkWidget *dialog;
	gpointer parent;

	parent = e_shell_view_get_shell_window (shell_view);

	dialog = em_folder_selector_create_new (
		parent, em_folder_tree_model_get_default (), 0,
		_("Create folder"), _("Specify where to create the folder:"));

	selector = EM_FOLDER_SELECTOR (dialog);
	folder_tree = em_folder_selector_get_folder_tree (selector);

	g_signal_connect (
		dialog, "response",
		G_CALLBACK (new_folder_response),
		gtk_tree_view_get_model (GTK_TREE_VIEW (folder_tree)));

	gtk_window_set_title (GTK_WINDOW (dialog), "New Shared Folder" );
	gtk_widget_show (dialog);
}

GtkWidget *
org_gnome_shared_folder_factory (EPlugin *ep,
                                 EConfigHookItemFactoryData *hook_data)
{
	gchar *id = NULL;
	const gchar *folder_name;
	EGwConnection *cnc;
	ShareFolder *sharing_tab;
	EMConfigTargetFolder *target=  (EMConfigTargetFolder *) hook_data->config->target;
	CamelFolder *folder = target->folder;
	CamelProvider *provider;
	CamelStore *store;

	folder_name = camel_folder_get_full_name (folder);

	store = camel_folder_get_parent_store (folder);
	provider = camel_service_get_provider (CAMEL_SERVICE (store));

	if (g_strcmp0 (provider->protocol, "groupwise") != 0)
		return NULL;

	 /* This is kind of bad..but we don't have types for all these folders.*/

	if ( !( strcmp (folder_name, "Mailbox")
	     && strcmp (folder_name, "Calendar")
	     && strcmp (folder_name, "Contacts")
	     && strcmp (folder_name, "Documents")
	     && strcmp (folder_name, "Authored")
	     && strcmp (folder_name, "Default Library")
	     && strcmp (folder_name, "Work In Progress")
	     && strcmp (folder_name, "Cabinet")
	     && strcmp (folder_name, "Sent Items")
	     && strcmp (folder_name, "Trash")
	     && strcmp (folder_name, "Checklist"))) {

		return NULL;
	}

	cnc = get_cnc (store);

	if (E_IS_GW_CONNECTION (cnc))
		/* XXX Are we leaking folder_name?  I can't tell. */
		id = get_container_id (cnc, g_strdup (folder_name));
	else
		g_warning("Could not Connnect\n");

	if (cnc && id)
		sharing_tab = share_folder_new (cnc, id);
	else
		return NULL;

	gtk_notebook_append_page((GtkNotebook *) hook_data->parent, (GtkWidget *) sharing_tab->vbox, gtk_label_new_with_mnemonic N_("Sharing"));
	common = sharing_tab;

	return GTK_WIDGET (sharing_tab);
}

EGwConnection *
get_cnc (CamelStore *store)
{
	const gchar *uri, *server_name, *user;
	CamelService *service;
	CamelSettings *settings;
	CamelNetworkSecurityMethod security_method;
	const gchar *scheme;
	const gchar *password;
	gchar *soap_port;

	if (!store)
		return  NULL;

	service = CAMEL_SERVICE (store);
	password = camel_service_get_password (service);
	settings = camel_service_get_settings (service);

	g_object_get (
		settings,
		"user", &user,
		"host", &server_name,
		"soap-port", &soap_port,
		"security-method", &security_method,
		NULL);

	if (security_method == CAMEL_NETWORK_SECURITY_METHOD_NONE)
		scheme = "http";
	else
		scheme = "https";

	uri = g_strdup_printf (
		"%s://%s:%s/soap", scheme, server_name, soap_port);

	return e_gw_connection_new (uri, user, password);
}

gchar *
get_container_id (EGwConnection *cnc,
                  const gchar *fname)
{
	GList *container_list = NULL;
	gchar *id = NULL;
	gchar *name;
	gchar **names;
	gint i = 0, parts = 0;

	names = g_strsplit (fname, "/", -1);
	if (names) {
		while (names[parts])
			parts++;
		fname = names[i];
	}

	/* get list of containers */
	if (e_gw_connection_get_container_list (cnc, "folders", &(container_list)) == E_GW_CONNECTION_STATUS_OK) {
		GList *container = NULL;

		for (container = container_list; container != NULL; container = container->next) {
			name = g_strdup (e_gw_container_get_name (container->data));
			/* if Null is passed as name then we return top lavel id*/
			if (fname == NULL) {
				id = g_strdup (e_gw_container_get_id (container->data));
				break;
			} else if (!strcmp (name, fname)) {
				if (i == parts - 1) {
					id = g_strdup (e_gw_container_get_id (container->data));
					break;
				} else
					fname = names[++i];
			}
			g_free (name);
		}
		e_gw_connection_free_container_list (container_list);
	}

	if (names)
		g_strfreev (names);
	return id;
}
