/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* camel-groupwise-transport.c : class for an groupwise transport */

/*
 * Authors: Sivaiah Nallagatla <snallagatla@novell.com>
 *	    Parthasarathi Susarla <sparthasarathi@novell.com>
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 * USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n-lib.h>

#include "camel-gwsend-transport.h"
#include "camel-gwsend-settings.h"
#include "camel-groupwise-transport.h"
#include "camel-groupwise-utils.h"

#define CAMEL_GWSEND_TRANSPORT_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), CAMEL_TYPE_GWSEND_TRANSPORT, CamelGwSendTransportPrivate))

G_DEFINE_TYPE (CamelGwSendTransport, camel_gwsend_transport, CAMEL_TYPE_GROUPWISE_TRANSPORT)

struct _CamelGwSendTransportPrivate {
	EGwConnection *cnc;
};

static void
gwsend_transport_dispose (GObject *object)
{
	CamelGwSendTransportPrivate *priv;

	priv = CAMEL_GWSEND_TRANSPORT_GET_PRIVATE (object);

	if (priv->cnc) {
		g_object_unref (priv->cnc);
		priv->cnc = NULL;
	}

	/* Chain up to parent's dispose() method. */
	G_OBJECT_CLASS (camel_gwsend_transport_parent_class)->dispose (object);
}

static gchar *
gwsend_transport_get_name (CamelService *service,
                              gboolean brief)
{
	CamelNetworkSettings *network_settings;
	CamelSettings *settings;
	const gchar *host;

	settings = camel_service_get_settings (service);

	network_settings = CAMEL_NETWORK_SETTINGS (settings);
	host = camel_network_settings_get_host (network_settings);

	if (brief)
		return g_strdup_printf (
			_("GroupWise server %s"), host);
	else
		return g_strdup_printf (
			_("GroupWise mail delivery via %s"), host);
}

static gboolean
check_for_connection (CamelService *service,
                      GCancellable *cancellable,
                      GError **error)
{
	CamelNetworkSettings *network_settings;
	CamelSettings *settings;
	const gchar *host;
	struct addrinfo hints, *ai;
	GError *local_error = NULL;

	settings = camel_service_get_settings (service);

	network_settings = CAMEL_NETWORK_SETTINGS (settings);
	host = camel_network_settings_get_host (network_settings);

	memset (&hints, 0, sizeof (hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_family = PF_UNSPEC;
	ai = camel_getaddrinfo(host, "groupwise", &hints, cancellable, &local_error);
	if (ai == NULL && g_error_matches (local_error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
		gchar *port_string;
		guint16 soap_port;

		g_clear_error (&local_error);

		soap_port = camel_network_settings_get_port (network_settings);

		port_string = g_strdup_printf ("%d", soap_port);
		ai = camel_getaddrinfo (
			host, port_string, &hints,
			cancellable, &local_error);
		g_free (port_string);
	}

	if (ai == NULL) {
		g_propagate_error (error, local_error);
		return FALSE;
	}

	camel_freeaddrinfo (ai);

	return TRUE;

}

static CamelAuthenticationResult
gwsend_authenticate_sync (CamelService *service,
                          const gchar *mechanism,
                          GCancellable *cancellable,
                          GError **error)
{
	CamelGwSendTransport *gw_transport = CAMEL_GWSEND_TRANSPORT (service);
	CamelGwSendTransportPrivate *priv = CAMEL_GWSEND_TRANSPORT_GET_PRIVATE (gw_transport);
	CamelNetworkSettings *network_settings;
	CamelNetworkSecurityMethod method;
	CamelAuthenticationResult result;
	CamelSettings *settings;
	gchar *uri;
	const gchar *host;
	const gchar *user;
	const gchar *scheme;
	const gchar *password;
	EGwConnectionErrors errors = {E_GW_CONNECTION_STATUS_INVALID_OBJECT, NULL};
	guint16 soap_port;

	password = camel_service_get_password (service);
	settings = camel_service_get_settings (service);

	if (password == NULL) {
		g_set_error_literal (
			error, CAMEL_SERVICE_ERROR,
			CAMEL_SERVICE_ERROR_CANT_AUTHENTICATE,
			_("Authentication password not available"));
		return CAMEL_AUTHENTICATION_ERROR;
	}

	network_settings = CAMEL_NETWORK_SETTINGS (settings);
	host = camel_network_settings_get_host (network_settings);
	user = camel_network_settings_get_user (network_settings);
	method = camel_network_settings_get_security_method (network_settings);

	soap_port = camel_network_settings_get_port (network_settings);

	if (method == CAMEL_NETWORK_SECURITY_METHOD_NONE)
		scheme = "http";
	else
		scheme = "https";

	uri = g_strdup_printf (
		"%s://%s:%d/soap", scheme, host, soap_port);

	priv->cnc = e_gw_connection_new_with_error_handler (
		uri, user, password, &errors);

	g_free (uri);

	if (E_IS_GW_CONNECTION (priv->cnc)) {
		result = CAMEL_AUTHENTICATION_ACCEPTED;
	} else if (errors.status == E_GW_CONNECTION_STATUS_INVALID_PASSWORD) {
		result = CAMEL_AUTHENTICATION_REJECTED;
	} else {
		g_set_error (
			error, CAMEL_SERVICE_ERROR,
			CAMEL_SERVICE_ERROR_UNAVAILABLE,
			"%s", errors.description ?
			errors.description :
			_("You must be working online "
			  "to complete this operation"));
		result = CAMEL_AUTHENTICATION_ERROR;
	}

	return result;
}

static gboolean
gwsend_connect_sync (CamelService *service,
                        GCancellable *cancellable,
                        GError **error)
{
	CamelGwSendTransport *gw_transport = CAMEL_GWSEND_TRANSPORT (service);
	CamelGwSendTransportPrivate *priv = CAMEL_GWSEND_TRANSPORT_GET_PRIVATE (gw_transport);
	CamelServiceConnectionStatus status;
	CamelSession *session;

	session = camel_service_get_session (service);
	status = camel_service_get_connection_status (service);

	if (status == CAMEL_SERVICE_DISCONNECTED)
		return FALSE;

	if (priv->cnc)
		return TRUE;

	if (!check_for_connection (service, cancellable, error)) {
		camel_service_disconnect_sync (service, TRUE, cancellable, NULL);
		return FALSE;
	}

	if (!camel_session_authenticate_sync (session, service, NULL, cancellable, error)) {
		camel_service_disconnect_sync (service, TRUE, cancellable, NULL);
		return FALSE;
	}

	if (!e_gw_connection_get_version (priv->cnc)) {
		camel_session_alert_user (session,
				CAMEL_SESSION_ALERT_WARNING,
				_("Some features may not work correctly with your current server version"),
				FALSE);

	}

	if (E_IS_GW_CONNECTION (priv->cnc)) {
		camel_groupwise_transport_set_connection (service, priv->cnc);
		return TRUE;
	}

	return FALSE;
}

static gboolean
gwsend_disconnect_sync (CamelService *service,
                           gboolean clean,
                           GCancellable *cancellable,
                           GError **error)
{
	CamelGwSendTransport *gw_transport = CAMEL_GWSEND_TRANSPORT (service);

	if (clean) {
		if (gw_transport->priv && gw_transport->priv->cnc) {
			g_object_unref (gw_transport->priv->cnc);
			gw_transport->priv->cnc = NULL;
			camel_groupwise_transport_set_connection (service, NULL);
		}
	}

	/* groupwise_disconnect_cleanup (service, clean, ex); */
	return TRUE;
}

extern CamelServiceAuthType camel_groupwise_password_authtype;

static  GList *
gwsend_query_auth_types_sync (CamelService *service,
                              GCancellable *cancellable,
                              GError **error)
{
	GList *auth_types = NULL;

	auth_types = g_list_prepend (auth_types,  &camel_groupwise_password_authtype);
	return auth_types;
}

static void
camel_gwsend_transport_class_init (CamelGwSendTransportClass *class)
{
	GObjectClass *object_class;
	CamelServiceClass *service_class;

	g_type_class_add_private (
		class, sizeof (CamelGwSendTransportPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->dispose = gwsend_transport_dispose;

	service_class = CAMEL_SERVICE_CLASS (class);
	service_class->settings_type = CAMEL_TYPE_GWSEND_SETTINGS;
	service_class->get_name = gwsend_transport_get_name;
	service_class->connect_sync = gwsend_connect_sync;
	service_class->disconnect_sync = gwsend_disconnect_sync;
	service_class->authenticate_sync = gwsend_authenticate_sync;
	service_class->query_auth_types_sync = gwsend_query_auth_types_sync;
}

static void
camel_gwsend_transport_init (CamelGwSendTransport *transport)
{
	transport->priv = CAMEL_GWSEND_TRANSPORT_GET_PRIVATE (transport);
}
