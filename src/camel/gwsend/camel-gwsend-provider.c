/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* camel-groupwise-provider.c: GroupWise provider registration code */

/*
 *  Authors: Jeffrey Stedfast <fejj@ximian.com>
 *           Sivaiah Nallagatla <snallagatla@novell.com>
 *           Rodrigo Moya <rodrigo@ximian.com>
 *
 *  Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <glib/gi18n-lib.h>
#include <gmodule.h>

#include "camel-gwsend-transport.h"
#include "camel-groupwise-store.h"
#include "camel-groupwise-transport.h"

static void add_hash (guint *hash, gchar *s);
static guint gwsend_url_hash (gconstpointer key);
static gint check_equal (gchar *s1, gchar *s2);
static gint gwsend_url_equal (gconstpointer a, gconstpointer b);

CamelProviderPortEntry gwsend_port_entries[] = {
							{ 7191, N_("Default GroupWise port"), FALSE },
							{ 0, NULL, 0 }
						  };

static CamelProvider gwsend_provider = {
	"gwsend",
	N_("Novell GroupWise"),

	N_("For sending mails using soap protocol"),

	"mail",

	CAMEL_PROVIDER_IS_REMOTE | CAMEL_PROVIDER_IS_SOURCE |
	CAMEL_PROVIDER_IS_STORAGE | CAMEL_PROVIDER_SUPPORTS_SSL | CAMEL_PROVIDER_DISABLE_SENT_FOLDER,

	CAMEL_URL_NEED_USER | CAMEL_URL_NEED_HOST | CAMEL_URL_ALLOW_AUTH,

	NULL,

	gwsend_port_entries,

	/* ... */
};

extern CamelServiceAuthType camel_groupwise_password_authtype; /*for the query_auth_types function */

static gint
gwsend_auto_detect_cb (CamelURL *url,
                          GHashTable **auto_detected,
                          GError **error)
{
	*auto_detected = g_hash_table_new (g_str_hash, g_str_equal);

	g_hash_table_insert (*auto_detected, g_strdup ("poa"),
			     g_strdup (url->host));

	return 0;
}

void
camel_provider_module_init (void)
{
	gwsend_provider.url_hash = gwsend_url_hash;
	gwsend_provider.url_equal = gwsend_url_equal;
	gwsend_provider.auto_detect = gwsend_auto_detect_cb;
	gwsend_provider.authtypes = g_list_prepend (gwsend_provider.authtypes, &camel_groupwise_password_authtype);
	gwsend_provider.translation_domain = GETTEXT_PACKAGE;

	gwsend_provider.object_types[CAMEL_PROVIDER_TRANSPORT] = camel_gwsend_transport_get_type ();

	camel_provider_register (&gwsend_provider);
}

static void
add_hash (guint *hash,
          gchar *s)
{
	if (s)
		*hash ^= g_str_hash(s);
}

static guint
gwsend_url_hash (gconstpointer key)
{
	const CamelURL *u = (CamelURL *) key;
	guint hash = 0;

	add_hash (&hash, u->user);
	add_hash (&hash, u->host);
	hash ^= u->port;

	return hash;
}

static gint
check_equal (gchar *s1,
             gchar *s2)
{
	if (s1 == NULL) {
		if (s2 == NULL)
			return TRUE;
		else
			return FALSE;
	}

	if (s2 == NULL)
		return FALSE;

	return strcmp (s1, s2) == 0;
}

static gint
gwsend_url_equal (gconstpointer a,
                     gconstpointer b)
{
	const CamelURL *u1 = a, *u2 = b;

	return check_equal (u1->protocol, u2->protocol)
		&& check_equal (u1->user, u2->user)
		&& check_equal (u1->host, u2->host)
		&& u1->port == u2->port;
}
