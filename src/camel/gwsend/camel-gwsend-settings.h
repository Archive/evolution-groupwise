/*
 * camel-gwsend-settings.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 */

#ifndef CAMEL_GWSEND_SETTINGS_H
#define CAMEL_GWSEND_SETTINGS_H

#include <camel/camel.h>

/* Standard GObject macros */
#define CAMEL_TYPE_GWSEND_SETTINGS \
	(camel_gwsend_settings_get_type ())
#define CAMEL_GWSEND_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), CAMEL_TYPE_GWSEND_SETTINGS, CamelGwSendSettings))
#define CAMEL_GWSEND_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), CAMEL_TYPE_GWSEND_SETTINGS, CamelGwSendSettingsClass))
#define CAMEL_IS_GWSEND_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), CAMEL_TYPE_GWSEND_SETTINGS))
#define CAMEL_IS_GWSEND_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), CAMEL_TYPE_GWSEND_SETTINGS))
#define CAMEL_GWSEND_SETTINGS_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), CAMEL_TYPE_GWSEND_SETTINGS, CamelGwSendSettingsClass))

G_BEGIN_DECLS

typedef struct _CamelGwSendSettings CamelGwSendSettings;
typedef struct _CamelGwSendSettingsClass CamelGwSendSettingsClass;
typedef struct _CamelGwSendSettingsPrivate CamelGwSendSettingsPrivate;

struct _CamelGwSendSettings {
	CamelSettings parent;
	CamelGwSendSettingsPrivate *priv;
};

struct _CamelGwSendSettingsClass {
	CamelSettingsClass parent_class;
};

GType		camel_gwsend_settings_get_type	(void) G_GNUC_CONST;

G_END_DECLS

#endif /* CAMEL_GWSEND_SETTINGS_H */
