/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* camel-imap-store.h : class for an imap store */

/*
 * Authors: Michael Zucchi <notzed@ximian.com>
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef CAMEL_GWIMAP_STORE_H
#define CAMEL_GWIMAP_STORE_H

#include <camel/camel.h>

/* Standard GObject macros */
#define CAMEL_TYPE_GWIMAP_STORE \
	(camel_gwimap_store_get_type ())
#define CAMEL_GWIMAP_STORE(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), CAMEL_TYPE_GWIMAP_STORE, CamelGwImapStore))
#define CAMEL_GWIMAP_STORE_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), CAMEL_TYPE_GWIMAP_STORE, CamelGwImapStoreClass))
#define CAMEL_IS_GWIMAP_STORE(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), CAMEL_TYPE_GWIMAP_STORE))
#define CAMEL_IS_GWIMAP_STORE_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), CAMEL_TYPE_GWIMAP_STORE))
#define CAMEL_GWIMAP_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), CAMEL_TYPE_GWIMAP_STORE, CamelGwImapStoreClass))

G_BEGIN_DECLS

typedef struct _CamelGwImapStore CamelGwImapStore;
typedef struct _CamelGwImapStoreClass CamelGwImapStoreClass;
typedef struct _CamelGwImapStorePrivate CamelGwImapStorePrivate;

struct _CamelGwImapStore {
	CamelIMAPXStore parent;
};

struct _CamelGwImapStoreClass {
	CamelIMAPXStoreClass parent_class;
};

GType		camel_gwimap_store_get_type	(void);

G_END_DECLS

#endif /* CAMEL_GWIMAP_STORE_H */
