/*
 * camel-groupwise-settings.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 */

#ifndef CAMEL_GWIMAP_SETTINGS_H
#define CAMEL_GWIMAP_SETTINGS_H

#include <camel/camel.h>

/* Standard GObject macros */
#define CAMEL_TYPE_GWIMAP_SETTINGS \
	(camel_gwimap_settings_get_type ())
#define CAMEL_GWIMAP_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), CAMEL_TYPE_GWIMAP_SETTINGS, CamelGwImapSettings))
#define CAMEL_GWIMAP_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), CAMEL_TYPE_GWIMAP_SETTINGS, CamelGwImapSettingsClass))
#define CAMEL_IS_GWIMAP_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), CAMEL_TYPE_GWIMAP_SETTINGS))
#define CAMEL_IS_GWIMAP_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), CAMEL_TYPE_GWIMAP_SETTINGS))
#define CAMEL_GWIMAP_SETTINGS_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), CAMEL_TYPE_GWIMAP_SETTINGS, CamelGwImapSettingsClass))

G_BEGIN_DECLS

typedef struct _CamelGwImapSettings CamelGwImapSettings;
typedef struct _CamelGwImapSettingsClass CamelGwImapSettingsClass;
typedef struct _CamelGwImapSettingsPrivate CamelGwImapSettingsPrivate;

struct _CamelGwImapSettings {
	CamelIMAPXSettings parent;
	CamelGwImapSettingsPrivate *priv;
};

struct _CamelGwImapSettingsClass {
	CamelIMAPXSettingsClass parent_class;
};

GType		camel_gwimap_settings_get_type
					(void) G_GNUC_CONST;

const gchar *	camel_gwimap_settings_get_soap_port
					(CamelGwImapSettings *settings);
void		camel_gwimap_settings_set_soap_port
					(CamelGwImapSettings *settings,
					 const gchar *soap_port);

G_END_DECLS

#endif /* CAMEL_GWIMAP_SETTINGS_H */
