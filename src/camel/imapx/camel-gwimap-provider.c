/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* camel-pop3-provider.c: pop3 provider registration code */
/*
 * Authors :
 *   Dan Winship <danw@ximian.com>
 *   Michael Zucchi <notzed@ximian.com>
 *
 * Copyright (C) 1999-2008 Novell, Inc. (www.novell.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <camel/camel.h>
#include <glib/gi18n-lib.h>

#include <camel-gwimap-store.h>

static guint gwimap_url_hash (gconstpointer key);
static gint  gwimap_url_equal (gconstpointer a, gconstpointer b);

CamelProviderConfEntry gwimap_conf_entries[] = {
	{ CAMEL_PROVIDER_CONF_SECTION_START, "mailcheck", NULL,
	  N_("Checking for New Mail") },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "check-all", NULL,
	  N_("C_heck for new messages in all folders"), "1" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "check-subscribed", NULL,
	  N_("Ch_eck for new messages in subscribed folders"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "use-qresync", NULL,
	  N_("Use _Quick Resync if the server supports it"), "1" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "use-idle", NULL,
	  N_("_Listen for server change notifications"), "1" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },
#ifndef G_OS_WIN32
	{ CAMEL_PROVIDER_CONF_SECTION_START, "cmdsection", NULL,
	  N_("Connection to Server") },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "use-shell-command", NULL,
	  N_("_Use custom command to connect to server"), "0" },
	{ CAMEL_PROVIDER_CONF_ENTRY, "shell-command", "use-shell-command",
	  N_("Command:"), "ssh -C -l %u %h exec /usr/sbin/dovecot --exec-mail imap" },
	{ CAMEL_PROVIDER_CONF_CHECKSPIN, "concurrent-connections", NULL,
	  N_("Numbe_r of cached connections to use"), "y:1:5:7" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },
#endif
	{ CAMEL_PROVIDER_CONF_SECTION_START, "folders", NULL,
	  N_("Folders") },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "use-subscriptions", NULL,
	  N_("_Show only subscribed folders"), "1" },
#if 0
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "use-namespace", NULL,
	  N_("O_verride server-supplied folder namespace"), "0" },
	{ CAMEL_PROVIDER_CONF_ENTRY, "namespace", "use-namespace",
	  N_("Namespace:") },
#endif
	{ CAMEL_PROVIDER_CONF_SECTION_END },
	{ CAMEL_PROVIDER_CONF_SECTION_START, "general", NULL, N_("Options") },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter-all", NULL,
	  N_("Apply _filters to new messages in all folders"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter-inbox", "!filter-all",
	  N_("_Apply filters to new messages in Inbox on this server"), "1" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter-junk", NULL,
	  N_("Check new messages for _Junk contents"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "filter-junk-inbox", "filter-junk",
	  N_("Only check for Junk messages in the IN_BOX folder"), "0" },
	{ CAMEL_PROVIDER_CONF_CHECKBOX, "stay-synchronized", NULL,
	  N_("Automatically synchroni_ze remote mail locally"), "0" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },

	/* extra GroupWise  configuration settings */
	{CAMEL_PROVIDER_CONF_SECTION_START, "soapport", NULL,
	  N_("SOAP Settings") },

	{ CAMEL_PROVIDER_CONF_ENTRY, "soap-port", NULL,
	  N_("Post Office Agent SOAP _Port:"), "7191" },

	{ CAMEL_PROVIDER_CONF_HIDDEN, "auth-domain", NULL,
	  NULL, "Groupwise" },
	{ CAMEL_PROVIDER_CONF_SECTION_END },

	{ CAMEL_PROVIDER_CONF_END }
};

CamelProviderPortEntry gwimap_port_entries[] = {
	{ 143, N_("Default IMAP port"), FALSE },
	{ 993, N_("IMAP over SSL"), TRUE },
	{ 0, NULL, 0 }
};

static CamelProvider gwimap_provider = {
	"gwimap",

	N_("GroupWise IMAP"),

	N_("For Accessing Novell GroupWise Server using imap for mails and groupwise protocol for calendar and address-book"),

	"mail",

	CAMEL_PROVIDER_IS_REMOTE | CAMEL_PROVIDER_IS_SOURCE |
	CAMEL_PROVIDER_IS_STORAGE | CAMEL_PROVIDER_SUPPORTS_SSL|
	CAMEL_PROVIDER_SUPPORTS_MOBILE_DEVICES |
	CAMEL_PROVIDER_SUPPORTS_BATCH_FETCH |
	CAMEL_PROVIDER_SUPPORTS_PURGE_MESSAGE_CACHE,

	CAMEL_URL_NEED_USER | CAMEL_URL_NEED_HOST | CAMEL_URL_ALLOW_AUTH,

	gwimap_conf_entries,

	gwimap_port_entries,

	/* ... */
};

void camel_gwimap_module_init (void);

extern CamelServiceAuthType camel_imapx_password_authtype;

void
camel_gwimap_module_init (void)
{
	gwimap_provider.object_types[CAMEL_PROVIDER_STORE] = camel_gwimap_store_get_type ();
	gwimap_provider.url_hash = gwimap_url_hash;
	gwimap_provider.url_equal = gwimap_url_equal;
	gwimap_provider.authtypes = camel_sasl_authtype_list (FALSE);
	gwimap_provider.authtypes = g_list_prepend (gwimap_provider.authtypes, &camel_imapx_password_authtype);
	gwimap_provider.translation_domain = GETTEXT_PACKAGE;

	/* TEMPORARY */
	imapx_utils_init ();

	camel_provider_register (&gwimap_provider);
}

void
camel_provider_module_init (void)
{
	camel_gwimap_module_init ();
}

static void
gwimap_add_hash (guint *hash,
                gchar *s)
{
	if (s)
		*hash ^= g_str_hash(s);
}

static guint
gwimap_url_hash (gconstpointer key)
{
	const CamelURL *u = (CamelURL *) key;
	guint hash = 0;

	gwimap_add_hash (&hash, u->user);
	gwimap_add_hash (&hash, u->host);
	hash ^= u->port;

	return hash;
}

static gint
gwimap_check_equal (gchar *s1,
                   gchar *s2)
{
	if (s1 == NULL) {
		if (s2 == NULL)
			return TRUE;
		else
			return FALSE;
	}

	if (s2 == NULL)
		return FALSE;

	return strcmp (s1, s2) == 0;
}

static gint
gwimap_url_equal (gconstpointer a,
                 gconstpointer b)
{
	const CamelURL *u1 = a, *u2 = b;

	return gwimap_check_equal (u1->protocol, u2->protocol)
		&& gwimap_check_equal (u1->user, u2->user)
		&& gwimap_check_equal (u1->host, u2->host)
		&& u1->port == u2->port;
}
