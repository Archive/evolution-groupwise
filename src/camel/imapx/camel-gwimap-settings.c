/*
 * camel-gwimap-settings.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 */

#include "camel-gwimap-settings.h"

#define CAMEL_GWIMAP_SETTINGS_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), CAMEL_TYPE_GWIMAP_SETTINGS, CamelGwImapSettingsPrivate))

struct _CamelGwImapSettingsPrivate {
	gchar *soap_port;
};

enum {
	PROP_0,
	PROP_SOAP_PORT
};

G_DEFINE_TYPE (
	CamelGwImapSettings,
	camel_gwimap_settings,
	CAMEL_TYPE_IMAPX_SETTINGS)

static void
gwimap_settings_set_property (GObject *object,
                                 guint property_id,
                                 const GValue *value,
                                 GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_SOAP_PORT:
			camel_gwimap_settings_set_soap_port (
				CAMEL_GWIMAP_SETTINGS (object),
				g_value_get_string (value));
			return;

	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gwimap_settings_get_property (GObject *object,
                                 guint property_id,
                                 GValue *value,
                                 GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_SOAP_PORT:
			g_value_set_string (
				value,
				camel_gwimap_settings_get_soap_port (
				CAMEL_GWIMAP_SETTINGS (object)));
			return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gwimap_settings_finalize (GObject *object)
{
	CamelGwImapSettingsPrivate *priv;

	priv = CAMEL_GWIMAP_SETTINGS_GET_PRIVATE (object);

	g_free (priv->soap_port);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (camel_gwimap_settings_parent_class)->finalize (object);
}

static void
camel_gwimap_settings_class_init (CamelGwImapSettingsClass *class)
{
	GObjectClass *object_class;

	g_type_class_add_private (class, sizeof (CamelGwImapSettingsPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->set_property = gwimap_settings_set_property;
	object_class->get_property = gwimap_settings_get_property;
	object_class->finalize = gwimap_settings_finalize;

	g_object_class_install_property (
		object_class,
		PROP_SOAP_PORT,
		g_param_spec_string (
			"soap-port",
			"SOAP Port",
			"Post Office Agent SOAP Port",
			"7191",
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));
}

static void
camel_gwimap_settings_init (CamelGwImapSettings *settings)
{
	settings->priv = CAMEL_GWIMAP_SETTINGS_GET_PRIVATE (settings);
}

const gchar *
camel_gwimap_settings_get_soap_port (CamelGwImapSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_GWIMAP_SETTINGS (settings), "7191");

	if (!settings->priv->soap_port)
		return "7191";

	return settings->priv->soap_port;
}

void
camel_gwimap_settings_set_soap_port (CamelGwImapSettings *settings,
                                        const gchar *soap_port)
{
	g_return_if_fail (CAMEL_IS_GWIMAP_SETTINGS (settings));

	if (g_strcmp0 (settings->priv->soap_port, soap_port) == 0)
		return;

	g_free (settings->priv->soap_port);
	if (atoi (soap_port))
		settings->priv->soap_port = g_strdup (soap_port);
	else
		settings->priv->soap_port = NULL;

	g_object_notify (G_OBJECT (settings), "soap-port");
}
