/*
 * camel-groupwise-settings.h
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 */

#ifndef CAMEL_GROUPWISE_SETTINGS_H
#define CAMEL_GROUPWISE_SETTINGS_H

#include <camel/camel.h>

/* Standard GObject macros */
#define CAMEL_TYPE_GROUPWISE_SETTINGS \
	(camel_groupwise_settings_get_type ())
#define CAMEL_GROUPWISE_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), CAMEL_TYPE_GROUPWISE_SETTINGS, CamelGroupwiseSettings))
#define CAMEL_GROUPWISE_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), CAMEL_TYPE_GROUPWISE_SETTINGS, CamelGroupwiseSettingsClass))
#define CAMEL_IS_GROUPWISE_SETTINGS(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), CAMEL_TYPE_GROUPWISE_SETTINGS))
#define CAMEL_IS_GROUPWISE_SETTINGS_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), CAMEL_TYPE_GROUPWISE_SETTINGS))
#define CAMEL_GROUPWISE_SETTINGS_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), CAMEL_TYPE_GROUPWISE_SETTINGS, CamelGroupwiseSettingsClass))

G_BEGIN_DECLS

typedef struct _CamelGroupwiseSettings CamelGroupwiseSettings;
typedef struct _CamelGroupwiseSettingsClass CamelGroupwiseSettingsClass;
typedef struct _CamelGroupwiseSettingsPrivate CamelGroupwiseSettingsPrivate;

struct _CamelGroupwiseSettings {
	CamelOfflineSettings parent;
	CamelGroupwiseSettingsPrivate *priv;
};

struct _CamelGroupwiseSettingsClass {
	CamelOfflineSettingsClass parent_class;
};

GType		camel_groupwise_settings_get_type
					(void) G_GNUC_CONST;
gboolean	camel_groupwise_settings_get_check_all
					(CamelGroupwiseSettings *settings);
void		camel_groupwise_settings_set_check_all
					(CamelGroupwiseSettings *settings,
					 gboolean check_all);
gboolean	camel_groupwise_settings_get_filter_junk
					(CamelGroupwiseSettings *settings);
void		camel_groupwise_settings_set_filter_junk
					(CamelGroupwiseSettings *settings,
					 gboolean filter_junk);
gboolean	camel_groupwise_settings_get_filter_junk_inbox
					(CamelGroupwiseSettings *settings);
void		camel_groupwise_settings_set_filter_junk_inbox
					(CamelGroupwiseSettings *settings,
					 gboolean filter_junk_inbox);
const gchar *	camel_groupwise_settings_get_soap_port
					(CamelGroupwiseSettings *settings);
void		camel_groupwise_settings_set_soap_port
					(CamelGroupwiseSettings *settings,
					 const gchar *soap_port);

G_END_DECLS

#endif /* CAMEL_GROUPWISE_SETTINGS_H */
