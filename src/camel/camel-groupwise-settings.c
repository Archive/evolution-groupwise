/*
 * camel-groupwise-settings.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the program; if not, see <http://www.gnu.org/licenses/>
 *
 */

#include "camel-groupwise-settings.h"

#define CAMEL_GROUPWISE_SETTINGS_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), CAMEL_TYPE_GROUPWISE_SETTINGS, CamelGroupwiseSettingsPrivate))

struct _CamelGroupwiseSettingsPrivate {
	gboolean check_all;
	gboolean filter_junk;
	gboolean filter_junk_inbox;
	gchar *soap_port;
};

enum {
	PROP_0,
	PROP_AUTH_MECHANISM,
	PROP_CHECK_ALL,
	PROP_FILTER_JUNK,
	PROP_FILTER_JUNK_INBOX,
	PROP_HOST,
	PROP_PORT,
	PROP_SECURITY_METHOD,
	PROP_SOAP_PORT,
	PROP_USER
};

G_DEFINE_TYPE_WITH_CODE (
	CamelGroupwiseSettings,
	camel_groupwise_settings,
	CAMEL_TYPE_OFFLINE_SETTINGS,
	G_IMPLEMENT_INTERFACE (
		CAMEL_TYPE_NETWORK_SETTINGS, NULL))

static void
groupwise_settings_set_property (GObject *object,
                                 guint property_id,
                                 const GValue *value,
                                 GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_AUTH_MECHANISM:
			camel_network_settings_set_auth_mechanism (
				CAMEL_NETWORK_SETTINGS (object),
				g_value_get_string (value));
			return;

		case PROP_CHECK_ALL:
			camel_groupwise_settings_set_check_all (
				CAMEL_GROUPWISE_SETTINGS (object),
				g_value_get_boolean (value));
			return;

		case PROP_FILTER_JUNK:
			camel_groupwise_settings_set_filter_junk (
				CAMEL_GROUPWISE_SETTINGS (object),
				g_value_get_boolean (value));
			return;

		case PROP_FILTER_JUNK_INBOX:
			camel_groupwise_settings_set_filter_junk_inbox (
				CAMEL_GROUPWISE_SETTINGS (object),
				g_value_get_boolean (value));
			return;

		case PROP_HOST:
			camel_network_settings_set_host (
				CAMEL_NETWORK_SETTINGS (object),
				g_value_get_string (value));
			return;

		case PROP_PORT:
			camel_network_settings_set_port (
				CAMEL_NETWORK_SETTINGS (object),
				g_value_get_uint (value));
			return;

		case PROP_SECURITY_METHOD:
			camel_network_settings_set_security_method (
				CAMEL_NETWORK_SETTINGS (object),
				g_value_get_enum (value));
			return;

		case PROP_SOAP_PORT:
			camel_groupwise_settings_set_soap_port (
				CAMEL_GROUPWISE_SETTINGS (object),
				g_value_get_string (value));
			return;

		case PROP_USER:
			camel_network_settings_set_user (
				CAMEL_NETWORK_SETTINGS (object),
				g_value_get_string (value));
			return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
groupwise_settings_get_property (GObject *object,
                                 guint property_id,
                                 GValue *value,
                                 GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_AUTH_MECHANISM:
			g_value_set_string (
				value,
				camel_network_settings_get_auth_mechanism (
				CAMEL_NETWORK_SETTINGS (object)));
			return;

		case PROP_CHECK_ALL:
			g_value_set_boolean (
				value,
				camel_groupwise_settings_get_check_all (
				CAMEL_GROUPWISE_SETTINGS (object)));
			return;

		case PROP_FILTER_JUNK:
			g_value_set_boolean (
				value,
				camel_groupwise_settings_get_filter_junk (
				CAMEL_GROUPWISE_SETTINGS (object)));
			return;

		case PROP_FILTER_JUNK_INBOX:
			g_value_set_boolean (
				value,
				camel_groupwise_settings_get_filter_junk_inbox (
				CAMEL_GROUPWISE_SETTINGS (object)));
			return;

		case PROP_HOST:
			g_value_set_string (
				value,
				camel_network_settings_get_host (
				CAMEL_NETWORK_SETTINGS (object)));
			return;

		case PROP_PORT:
			g_value_set_uint (
				value,
				camel_network_settings_get_port (
				CAMEL_NETWORK_SETTINGS (object)));
			return;

		case PROP_SECURITY_METHOD:
			g_value_set_enum (
				value,
				camel_network_settings_get_security_method (
				CAMEL_NETWORK_SETTINGS (object)));
			return;

		case PROP_SOAP_PORT:
			g_value_set_string (
				value,
				camel_groupwise_settings_get_soap_port (
				CAMEL_GROUPWISE_SETTINGS (object)));
			return;

		case PROP_USER:
			g_value_set_string (
				value,
				camel_network_settings_get_user (
				CAMEL_NETWORK_SETTINGS (object)));
			return;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
groupwise_settings_finalize (GObject *object)
{
	CamelGroupwiseSettingsPrivate *priv;

	priv = CAMEL_GROUPWISE_SETTINGS_GET_PRIVATE (object);

	g_free (priv->soap_port);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (camel_groupwise_settings_parent_class)->finalize (object);
}

static void
camel_groupwise_settings_class_init (CamelGroupwiseSettingsClass *class)
{
	GObjectClass *object_class;

	g_type_class_add_private (class, sizeof (CamelGroupwiseSettingsPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->set_property = groupwise_settings_set_property;
	object_class->get_property = groupwise_settings_get_property;
	object_class->finalize = groupwise_settings_finalize;

	/* Inherited from CamelNetworkSettings. */
	g_object_class_override_property (
		object_class,
		PROP_AUTH_MECHANISM,
		"auth-mechanism");

	g_object_class_install_property (
		object_class,
		PROP_CHECK_ALL,
		g_param_spec_boolean (
			"check-all",
			"Check All",
			"Check all folders for new messages",
			FALSE,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));

	g_object_class_install_property (
		object_class,
		PROP_FILTER_JUNK,
		g_param_spec_boolean (
			"filter-junk",
			"Filter Junk",
			"Whether to filter junk from all folders",
			FALSE,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));

	g_object_class_install_property (
		object_class,
		PROP_FILTER_JUNK_INBOX,
		g_param_spec_boolean (
			"filter-junk-inbox",
			"Filter Junk Inbox",
			"Whether to filter junk from Inbox only",
			FALSE,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));

	/* Inherited from CamelNetworkSettings. */
	g_object_class_override_property (
		object_class,
		PROP_HOST,
		"host");

	/* Inherited from CamelNetworkSettings. */
	g_object_class_override_property (
		object_class,
		PROP_PORT,
		"port");

	/* Inherited from CamelNetworkSettings. */
	g_object_class_override_property (
		object_class,
		PROP_SECURITY_METHOD,
		"security-method");

	g_object_class_install_property (
		object_class,
		PROP_SOAP_PORT,
		g_param_spec_string (
			"soap-port",
			"SOAP Port",
			"Post Office Agent SOAP Port",
			"7191",
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));

	/* Inherited from CamelNetworkSettings. */
	g_object_class_override_property (
		object_class,
		PROP_USER,
		"user");
}

static void
camel_groupwise_settings_init (CamelGroupwiseSettings *settings)
{
	settings->priv = CAMEL_GROUPWISE_SETTINGS_GET_PRIVATE (settings);
}

gboolean
camel_groupwise_settings_get_check_all (CamelGroupwiseSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings), FALSE);

	return settings->priv->check_all;
}

void
camel_groupwise_settings_set_check_all (CamelGroupwiseSettings *settings,
                                        gboolean check_all)
{
	g_return_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings));

	if ((settings->priv->check_all ? 1 : 0) == (check_all ? 1 : 0))
		return;

	settings->priv->check_all = check_all;

	g_object_notify (G_OBJECT (settings), "check-all");
}

gboolean
camel_groupwise_settings_get_filter_junk (CamelGroupwiseSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings), FALSE);

	return settings->priv->filter_junk;
}

void
camel_groupwise_settings_set_filter_junk (CamelGroupwiseSettings *settings,
                                          gboolean filter_junk)
{
	g_return_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings));

	if ((settings->priv->filter_junk ? 1 : 0) == (filter_junk ? 1 : 0))
		return;

	settings->priv->filter_junk = filter_junk;

	g_object_notify (G_OBJECT (settings), "filter-junk");
}

gboolean
camel_groupwise_settings_get_filter_junk_inbox (CamelGroupwiseSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings), FALSE);

	return settings->priv->filter_junk_inbox;
}

void
camel_groupwise_settings_set_filter_junk_inbox (CamelGroupwiseSettings *settings,
                                                gboolean filter_junk_inbox)
{
	g_return_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings));

	if ((settings->priv->filter_junk_inbox ? 1 : 0) == (filter_junk_inbox ? 1 : 0))
		return;

	settings->priv->filter_junk_inbox = filter_junk_inbox;

	g_object_notify (G_OBJECT (settings), "filter-junk-inbox");
}

const gchar *
camel_groupwise_settings_get_soap_port (CamelGroupwiseSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings), "7191");

	if (!settings->priv->soap_port)
		return "7191";

	return settings->priv->soap_port;
}

void
camel_groupwise_settings_set_soap_port (CamelGroupwiseSettings *settings,
                                        const gchar *soap_port)
{
	g_return_if_fail (CAMEL_IS_GROUPWISE_SETTINGS (settings));

	if (g_strcmp0 (settings->priv->soap_port, soap_port) == 0)
		return;

	g_free (settings->priv->soap_port);
	if (atoi (soap_port))
		settings->priv->soap_port = g_strdup (soap_port);
	else
		settings->priv->soap_port = NULL;

	g_object_notify (G_OBJECT (settings), "soap-port");
}
